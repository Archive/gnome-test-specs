#!/usr/bin/perl
#ident	"%Z%%Y%:%M%	%I%"

#
# Revision History
#
# 0001  Changed INTEL to 80386 and MSB to LSB for        prabhut  24/06/02
#       INTEL_STRING_BIN and INTEL_STRING_LIB.
#
#
$BINLIST="/opt/sanity/BinaryList";
$PKGLIST="/opt/sanity/Inspkglists";
#$PKGPATH="/var/spool/pkg/";
$RELOCPATH="/reloc";
$SPARC_STRING_BIN ="32-bit MSB executable SPARC";
$SPARC_STRING_LIB ="32-bit MSB dynamic lib SPARC";
#0001
$INTEL_STRING_BIN ="32-bit LSB executable 80386";
$INTEL_STRING_LIB ="32-bit LSB dynamic lib 80386";


#Main program starts here
$PKGPATH = $ARGV[0];

$ARCH=`uname -p`;
chomp($ARCH);

#Creat/open a binary list file for writing 
if(open(binlists,">$BINLIST") ne 1)
{
	die("Error in opening $BINLIST file \n");
}

#Open install package list for reading
if(open(pkglist,"$PKGLIST") ne 1) 
{
	die("$PKGLIST file does'nt exists\n");
}

print ("Getting the Binary List from Gnome-2.0 packages \n");

#Browse through pkglist file and list the architecture binaries.
while(($pname = <pkglist>) ne "")
{
	chomp($pname);	
	$progra="";
	$progra=$PKGPATH . $pname;
        $progra = $progra . $RELOCPATH; 
	$program=$progra . "/bin";  
	if(-d $program)
	{
		@list=`ls $program`;
		foreach(@list) 
		{	
			#List all the binaries under bin directory.
			$program = $progra . "/bin/$_"; 	
			$output = `file $program`;
			if($ARCH eq "sparc")
			{
				if(($output =~ /$SPARC_STRING_BIN/))  
				{
					print binlists "$pname/reloc/bin/$_";
        			} 
			}
			else 
			{
				if(($output =~ /$INTEL_STRING_BIN/) )  
				{
					print binlists "$pname/reloc/bin/$_";
        			} 
			}			
		} 
	}

	# List all the binaries under lib directory.
	$program=$progra . "/lib";  
	if(-d $program)
	{
		@list=`ls $program`;
		foreach(@list) 
		{
			$program = $progra . "/lib/$_"; 	
			$output = `file $program`;
			if($ARCH eq "sparc")
			{
				if(($output =~ /$SPARC_STRING_LIB/))  
				{
					print binlists "$pname/reloc/lib/$_";
				} 
			}
			else 
			{
				if(($output =~ /$INTEL_STRING_LIB/))  
				{
					print binlists "$pname/reloc/lib/$_";
        			} 
			}
		}
	}
}

print ("\nSuccessfully generated list of binaries in $BINLIST file\n");
