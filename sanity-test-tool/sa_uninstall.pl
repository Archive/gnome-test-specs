#!/usr/bin/perl
#ident	"%Z%%Y%:%M%	%I%"

#####################################################################
# NAME of the file - sanuninstall.pl
# 
# USAGE - sanuninstall.pl
# 
# DESCRIPTION - This program is Sanity uninstall script.
#		This program will remove the gnome-2.0
# 		packages that has been installed.
#
#
# 0001   Modified to handle solaris intel    prabu.thiagaraj@wipro.com 09/20/02
#        installation also.
#####################################################################

# Global Variables.

@pkg =();
$PKGRM  = "/usr/sbin/pkgrm";
$PKGINFO = "/usr/bin/pkginfo -l";
$PKGLIST = "/opt/sanity/Uninspkglists";
$ADMIN = "pkgrm.admin";
#0001
$ARCH="";

###########################################################################
# Name       : ReadPkgName
#
# Description: This function will read the names of the package from 
#              pkglists file, then stores it in global array variable "pkg".
#
############################################################################ 

sub ReadPkgName
{
	if(open(pkglists,"$PKGLIST"))
	{
		$i = 0;

		while(($pkg[$i] = <pkglists>) ne "")
		{
			chop($pkg[$i]);
                        $i++;
		}
	} 
	else
	{        
		die("File $PKGLIST not found \n");
	}
}

###########################################################################
# Name       : RemovePkg
#
# Description: To remove all the gnome-2.0 packages that has been installed.
#
########################################################################### 

sub RemovePkg
{
	$i = 0;

	$ARCH=`uname -p`;
	chomp($ARCH);

	while($pkg[$i] ne "")
	{
                # 0001 - Start
                if($ARCH eq "i386" )
                {
                        if ($pkg[$i] eq "SUNWgnome-removable-media")
                        {
                                $i++;
				next;
                        }
                }
                # 0001 - Ends

		print("$i:\t Removing Package $pkg[$i] .\n");
		if(system("$PKGRM -a $ADMIN -n $pkg[$i]") != 0)
		{
			die("Failed to issue `pkgrm` command.\n");
		}
		$i++;
	}
}


################################################################################ 
# Name        : "main"
#
# Description : This is main program .The execution of the program starts 
#		from here
#       
################################################################################ 


if(system("/usr/bin/clear") != 0)
{
	die("Failed to issue `clear` command");
}


# Read the packages name specific to architecture.
&ReadPkgName;

# Remove Package.
&RemovePkg;

