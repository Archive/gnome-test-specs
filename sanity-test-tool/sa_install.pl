#!/usr/bin/perl
#ident	"%Z%%Y%:%M%	%I%"

################################################################### 
# NAME of the file - saninstall.pl
# 
# USAGE       - saninstall.pl
# 
# DESCRIPTION - This program is Sanity install script.
#		This program will install the gnome-2.0
# 		packages to the host system.
#
# Revision History
#
# No     Description                       mail-id               (mm/dd/yyyy)
#
# 0001   Replaced i with $i       prabu.thiagaraj@wipro.com      09/16/2002
#
###################################################################

# Global Variables.

$ARCH = "";
@pkg=();

$PKGDEVCHK = "/usr/sbin/pkgchk -d";
$PKGINSCHK = "/usr/sbin/pkgchk";
# 0001 - Added -n in order to avoid error reporting for volatile files.
$PKGCHKVERB = "/usr/sbin/pkgchk -v -n";
$PKGADD = "/usr/sbin/pkgadd";
$PKGINFO = "/usr/bin/pkginfo -l";
$LDDCHECK = "/usr/bin/ldd -r";

$PKGPATH  = "";
$BINPATH = "/usr/bin/";
$LIBPATH = "/usr/lib/";
$PKGDIR = "/usr";

$ALIST = "/opt/sanity/Applicationlists";
$LLIST = "/opt/sanity/Librarylists";
$PKGLIST = "/opt/sanity/Inspkglists";
$ADMIN = "/opt/sanity/pkgadd.admin";
$BINLIST = "/opt/sanity/BinaryList";

#####################################################################
# Name        : ProcArch
#
# Description : To print the architecture of Processor on standard o/p.
#
###################################################################### 

sub ProcArch 
{
	$ARCH = `uname -p`;
        chomp($ARCH);
	if($ARCH eq "")
	{
		print("\t Processor Architecture type is not set \n");
	}
	else
	{
		print("\t Processor Architecture is $ARCH \n");
	}
	$SPARC_STRING_BIN ="$BITTYPE-bit MSB executable SPARC";
	$SPARC_STRING_LIB ="$BITTYPE-bit MSB dynamic lib SPARC";
	$INTEL_STRING_BIN ="$BITTYPE-bit MSB executable INTEL";
	$INTEL_STRING_LIB ="$BITTYPE-bit MSB dynamic lib INTEL";
	$SPARC_MESSAGE = "$BITTYPE-bit SPARC";
	$INTEL_MESSAGE = "$BITTYPE-bit INTEL"; 
}


########################################################################
# Name        : PreInstallCheck
#
# Description : This function will check for the install image of each
#               package in the package source directory (/var/spool/pkg), before
#               installing the package in the host system.
#
########################################################################

sub PreInstallCheck 
{
	# 0001 - If it is run on solaris intel, do not check the RMM package.
	$ARCH=`uname -p`;
        chomp($ARCH);

	print("\n\n\t Executing Test Case Sanity 01-01. \n\n");
	print("\n\tChecking for install image in the Package Directoy. \n");

	$i = 0;

	while($pkg[$i] ne "")
	{
		# 0001 - Start
		if($ARCH eq "i386" )
		{
			if ($pkg[$i] eq "SUNWgnome-removable-media")
			{
				$i++;
				next;
			}
		}

		if($pkg[$i] eq "SUNWgnome-dtlogin-integration")
		{
			$i++;
			next;
		}
		# 0001 - Ends

		if(system("$PKGDEVCHK $PKGPATH $pkg[$i] > /tmp/precheck 2>&1") != 0)
		{
			die("Failed to issue $PKGDEVCHK $pkg[$i] command");
		}

		$retvalue = `grep ERROR /tmp/precheck`;

		if($retvalue ne "")
		{
			die("$retvalue"); 
		}

		$i++;
	}

	print("\n\t Finished executing Test Case Sanity 01-01 \n\n");
}

######################################################################## 
# Name        : PostInstallCheck
#
# Description : This function will check for a package once the packages
#               has been installed in the host system.
#
######################################################################## 

sub PostInstallCheck
{
	print("\n\n\t Executing Test Case Sanity 01-03. \n\n");

	$ARCH=`uname -p`;
	chomp($ARCH);

        $i = 0;

        while($pkg[$i] ne "")
	{
		# 0001 - Start
		if($ARCH eq "i386" )
		{
			if ($pkg[$i] eq "SUNWgnome-removable-media")
			{
				$i++;
				next;
			}
		}

		if($pkg[$i] eq "SUNWgnome-dtlogin-integration")
		{
			$i++;
			next;
		}
		# 0001 - Ends

		print("\n Package: $pkg[$i] \n");

	       	if(system("$PKGCHKVERB $pkg[$i] > /tmp/postcheck 2>&1") != 0)
		{
			die("Failed to issue 'pkgchk -v' command for $pkg[$i]");
		}

		$retvalue = `grep WARNING /tmp/postcheck`;

                if($retvalue ne "")
                {
                        die("$retvalue"); 
                }

                $i++;
        }
	print("\n\n\t Finished executing Test Case Sanity 01-03 \n\n");
}

#########################################################################
# Name        : LogInstalledPkgInfo
#
# Description : This function will invoke pkginfo command on all the 
#               packages that has been installed and it logs the information
#               like package instance, status and version in log file.
#
########################################################################## 

sub LogInstalledPkgInfo
{
	print("\n\n\t Executing Test Case Sanity 01-04 \n\n");

	$ARCH=`uname -p`;
	chomp($ARCH);

	$i = 0;
	
	while($pkg[$i] ne "")
	{
                # 0001 - Start
                if($ARCH eq "i386" )
                {
                        if ($pkg[$i] eq "SUNWgnome-removable-media")
                        {
                                $i++;
				next;
                        }
                }

		if($pkg[$i] eq "SUNWgnome-dtlogin-integration")
		{
			$i++;
			next;
		}
                # 0001 - Ends

                print("Package $pkg[$i]\n");

		$retvalue = `$PKGINFO $pkg[$i] | grep STATUS`;

		$result = $retvalue =~ /completely/;

		if($result)
		{
        		print("\n Package $pkg[$i] installed completely.\n");
		}
		else
		{
        		die("Not completetly installed");
		}

                if(system("$PKGINFO $pkg[$i] | grep PKGINST >> /tmp/sanitylog 2>&1") != 0)
		{
			 die("Failed to issue 'pkginfo -l' command");
		}	

                if(system("$PKGINFO $pkg[$i] | grep STATUS >> /tmp/sanitylog 2>&1") != 0)
		{
			 die("Failed to issue 'pkginfo -l' command");
		}

                if(system("$PKGINFO $pkg[$i] | grep VERSION >> /tmp/sanitylog 2>&1") != 0)
		{
			 die("Failed to issue 'pkginfo -l' command");
		}

                if(system(" echo \"------------------------------------------------
---\" >> /tmp/sanitylog 2>&1") != 0)
		{
			die("Failed to issue 'pkginfo -l' command");
		}	

		$i++;
	}

	print("\n\n\t Finished executing Test Case Sanity 01-04 \n\n");

}

########################################################################
# Name        : ReadPkgName
#
# Description : This function will read the names of the package from 
#               pkglists file, then stores it in global array variable "pkg".
#
########################################################################## 

sub ReadPkgName 
{
        if(open(pkglists,"$PKGLIST"))
	{
         	$i = 0;

                while(($pkg[$i] = <pkglists>) ne "")
		{
			chop($pkg[$i]);
                        $i++;
		}
        }
	else
	{
		die("File $PKGLIST not found \n");

	}
}

##########################################################################
# Name       : AddPkg
#
# Description: This function will perform a pkgchk on each package instance
#              before adding to the system.  Then the package will be added
#              once the pkgchk completes successfully.
#
###########################################################################

sub AddPkg
{
	
	print("\n\n\t Executing Test Case  Sanity 01-02.\n");

	$ARCH=`uname -p`;
	chomp($ARCH);

	$i = 0;

	while($pkg[$i] ne "")
	{
                # 0001 - Start
                if($ARCH eq "i386" )
                {
                        if ($pkg[$i] eq "SUNWgnome-removable-media")
                        {
                                $i++;
				next;
                        }
                }

		if($pkg[$i] eq "SUNWgnome-dtlogin-integration")
		{
			$i++;
			next;
		}
                # 0001 - Ends

		print("\Before Adding, Checking for Package Existence..\n");

		if(system("$PKGINSCHK $pkg[$i] > /tmp/out 2>&1") != 0)
		{
			die("Failed to issue $PKGINSCHK command on $pkg[$i]\n");
		}

		if(open(file,"/tmp/out"))
		{
			$msg = <file>;

			if($msg eq "")
			{
				die("$msg\n Package $pkg[$i] Exists. Program Exited.");
			}
		}

		print("$i:\t Adding Package $pkg[$i] .\n");
		if(system("$PKGADD -a $ADMIN -n -d $PKGPATH $pkg[$i]") != 0) 
		{
			#0001
			die("Failed to install package $pkg[$i] ");
		}
				
		$i++;

	}

	print("\n\n\t Finished executing Test Case Sanity 01-02 \n\n");

}

######################################################################### 
# Function   : ComponentExistCheck
#
# Description: This function will check for the existence of diretory
#              /usr/gnome. Then it will call GetComponentList to
#              store the list of components in a variable.
#
######################################################################### 

sub ComponentExistCheck
{

	print("\n\n\t Executing Test Case Sanity 02-01. \n\n");

	print("\n\t\t Checking for existence of /usr/gnome directory \n");

	if(-d $PKGDIR) 
	{
		print("\n\t $PKGDIR directory exist.\n");
	} 
	else 
	{
		print("\n\t $PKGDIR directory doesn't exist.\n");

	}

	print("\n\n\t Finished executing Test Case Sanity 02-01\n\n");

	&GetComponentList;

}

########################################################################
# Name        : GetComponentList
#
# Description : This function will store the component lists in a variable
#               by reading from the file componentlists.
#
#########################################################################

sub GetComponentList
{

	print("\n\n\t Executing Test Case Sanity 02-02. \n\n");

        if(open(fname,$ALIST))
	{
                $i = 0;

                print("\t List in file $ALIST are:\n");

                while(($cname = <fname>) ne "")
		{
			print("\n\t $i: $cname ");

        		chop($cname);

        		$cname = $BINPATH . $cname;
			&lddCheck;
			$i++;
		}
	}
	else
	{
		die("File $ALIST doesn't exist \n");
	}

        if(open(fname,$LLIST))
	{
                print("\n\t List in file $LLIST are:\n");

                while(($cname = <fname>) ne "")
		{
			print("\n\t $i: $cname ");

        		chop($cname);

        		$cname = $LIBPATH . $cname;
			&lddCheck;
			$i++;
		}
	}
	else
	{
		die("File $LLIST doesn't exist \n");
	}

	print("\n\n\t Finished executing Test Case Sanity 02-02 \n\n");
}

######################################################################## 
# Name       : lddCheck
#
# Description: To perform ldd check on the component listed in the file.
#
######################################################################## 

sub lddCheck    
{
	print("\n\n\t Performing ldd -r on components.\n\n");


	if(-f $cname)
	{
		print("Component: \'$cname\' exist \n");
		print("Executing: $LDDCHECK $cname \n");

		$retvalue = `$LDDCHECK $cname | grep symbol`;

		$result = $retvalue =~ /symbol not found/;

		if($result)
		{
			print("Error: $retvalue");
		}
	}
	else
	{
		print("Component: \'$cname\' does not exist \n");

	}

}

#######################################################################
# Name       : CleanTempFiles
#
# Description: This function will delete the files that has been created 
#              temporarily.
#
########################################################################


sub CleanTempFiles 
{

	print("\n\n Cleaning the  files that has been created Temporarily \n");
	print("\n\t Deleting... \n");
	if(system("rm -f /tmp/out") != 0 )
	{
		die("Failed to issue 'rm -f /tmp/out' command");
	}

	if(system("rm -f /tmp/precheck") != 0 )
	{
		die("Failed to issue 'rm -f /tmp/precheck' command");
	}

	if(system("rm -f /tmp/postcheck") != 0 )
	{
		die("Failed to issue 'rm -f /tmp/postcheck' command");
	}
	print("\n\t Completed.\n");	 	

}

######################################################################
# Name		:BinaryCheck
# Description   : This is used to check the Architecture of the binaries
#                 in the build before installation 
#
####################################################################### 

sub BinaryCheck
{
	print ("Checking the Binary Information of the Packages \n");

	if(open(binlists,"$BINLIST"))
	{
		while(($bname = <binlists>) ne "")
		{
			$program="";
			$program=$PKGPATH . $bname;
			$output = `file $program`;
			chomp($bname);

			if($ARCH eq "sparc")
			{
				if($output !~ /$SPARC_STRING_BIN/ and $output !~ /$SPARC_STRING_LIB/)
				{
					die("Error-Wrong Architecture for the component $program \n ");
				}
				else
				{
					print("\n$bname is a $SPARC_MESSAGE file \n");
				}
			}
			else
			{
				if($output !~ /$INTEL_STRING_BIN/ and $output !~ /$INETEL_STRING_LIB/)
				{
					die("Error-Wrong Architecture for the component $program \n ");
				}
				else
				{
					print("\n$bname is a $INTEL_MESSAGE file\n");
				}
			}

		} 
	}
	else 
	{
		die("File $BINLIST not found \n");
	}
	print ("\nFinished Checking the Binary Information of the Packages \n");
} 
    

####################################################################### 
# Name        : "main"
#
# Description : This is main program .The execution of the program starts 
#		from here
#       
######################################################################## 

if(system("/usr/bin/clear") != 0)
{
	die("Failed to issue `clear` command");
}

$PKGPATH = $ARGV[0];
$BITTYPE = 32;

# To find the Processor Architecture.
&ProcArch;

# Read the packages name specific to architecture. 
&ReadPkgName;

# Package check on install image.
&PreInstallCheck;

#Checking for the architecture of the binaries
&BinaryCheck;

# To check arch specific pkg existence and add the packages.
&AddPkg;

# Package check on Post install image.
&PostInstallCheck;

# Log Package installation information.
&LogInstalledPkgInfo;

# Check for the existence of the components.
&ComponentExistCheck;

# Clean the Temporary files created.
&CleanTempFiles;
