#!/usr/bin/ksh
#ident	"%Z%%Y%:%M%	%I%"

#
# Revision History:
#
#0001 Installation of                   prabu.thiagaraj@wipro.com  09/20/2002
#     SUNWgnome-dtlogin-integration
#     package gives error.
#
#

OS=`uname -v |cut -f 1 -d "_"`
if [ "$OS" = 's9' ]
then
cp Inspkglists.S9 Inspkglists
fi

if [ "$OS" = "s10" ]
then
cp Inspkglists.S10 Inspkglists
fi


verify_package_built()
{
# Function to check if all the packages listed in Inspkglists is available
# or not. If any one of the package is not present, it will abort.

	rc=0

	for i in `cat /opt/sanity/Inspkglists`
	do
   		if [ ! -d $PKGDIR/$i ]
   		then
      			echo $i package is not built
			rc=1
   		fi
	done

	if [ $rc -eq 1 ]
	then
		echo "\n\nAbove mentioned package(s) are not built"
		echo "Do you want to continue (y/n)"
		read a

		if [ "$a" = "n" ]
		then
			exit
		fi
	fi
}

verify_package_in_Inspkglist()
{
# Function to check if all the packages built are available in
# /opt/sanity/Inspkglists file. If additional packages are available then this
# script will abort.

	cd $PKGDIR

	rc=0
	for i in `ls`
	do
		grep -w $i /opt/sanity/Inspkglists >/dev/null 2>&1

		if [ $? -ne 0 ]
		then
			echo $i package is missing in Inspkglists
			rc=1
		fi
	done

	if [ $rc -eq 1 ]
	then
		echo "\n\nAbove mentioned packages(s) are not listed in the Inspkglists file"
		echo "Do you want to continue (y/n)"
		read a

		if [ "$a" = "n" ]
		then
			exit
		fi
	fi
}

#main

if [ $# -eq 1 ]
then
	PKGDIR=$1
	PKGDIR=`echo $PKGDIR/`
	if [ ! -d $PKGDIR ]
	then
		echo $PKGDIR is not a directory
		exit
	fi
else
	PKGDIR=/var/spool/pkg/
fi

if [ ! -f /opt/sanity/Inspkglists ]
then
	echo "/opt/sanity/Inspkglists file is missing"
	exit
fi

chmod 555 *.pl

verify_package_built

verify_package_in_Inspkglist

export PATH=$PATH:/usr/bin
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib

# Create BinaryList file
echo "Creating BinaryList file"
/opt/sanity/sa_create_bin_list.pl $PKGDIR

# Create Uninspkglist file
echo "Creating Uninspkglists file"
/opt/sanity/sa_create_uninstall.pl

echo "Starting sanity installation. No manual intervention is required"
# Start the sanity installation.
/opt/sanity/sa_install.pl $PKGDIR 32

# 0001 - Since installation of this package gives error. Let us install
# manually.
if [ -d $PKGDIR/SUNWgnome-dtlogin-integration ]
then
   echo "Installing SUNWgnome-dtlogin-integration package manually :"
   pkgadd -n -a /opt/sanity/pkgadd.admin -d $PKGDIR SUNWgnome-dtlogin-integration
fi
