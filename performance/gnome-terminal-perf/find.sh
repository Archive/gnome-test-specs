#!/bin/ksh

# this script will infinitely loop the "find" command

# confirm user wants to run the script - warn that this is an infinite loop.
# if user wants to run the srcipt then declare integer x


integer x
file=find_output

print -n "This script is an infinite loop - are you sure you want to execute this script (ans)?"
read ans junk

while
 [[ $ans = [yY] ]] || [[ $ans = [yY]es ]]
do
 print -n "Ctrl-c will exit this script - are you sure you want to execute this script (confirm)?" 
 read confirm junk
 if  
  [[ $confirm = [yY] ]] || [[ $confirm = [yY]es ]]
 then 
  print -n "Do you want to remove the file \"$file\"?"
  read ans junk
  if
   [[ $ans = [yY] ]] || [[ $ans = [yY]es ]]
  then
   /usr/bin/rm -r $file
  fi
  clear
  print "Executing the \"find /\" loop - output will be written to the file \"find_output\"\n\nNothing will be displayed to terminal"
  sleep 1
  x=1
  while 
   (( x==1 ))
  do
   find / > $file 
   x=1
  done
 else
  break
 fi
done


