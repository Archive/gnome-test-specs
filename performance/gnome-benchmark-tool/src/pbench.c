#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <glib.h>

enum {INVALID_ARG, APP_LAUNCHING, DIR_SELECTION, WS_SWITCHING, SCREEN_SAVER};
enum {SUCCESS, FAILURE};

static char *whats_tested[] = { "Invalid", "launch", "dirsel", "wsswitch", NULL };

static char env_outfile [256];
static char env_date[256];
static char env_appname[256];
static char env_timer[128];
static char env_dirname[128];
static char app_full_path[256] = {"/usr/gnome/bin/"};
struct timeval starttime;
struct timezone tzp;

static void print_usage ()
{
	printf ("usage [1]: pbench -l app [timer-secs]\n"); 
	printf ("usage [2]: pbench -d dirname [timer-secs]\n"); 
	printf ("usage [3]: pbench -w path-of-test-pager-app\n"); 
	printf ("usage [4]: pbench -s (not supported)\n"); 
	printf ("See pbench.README for more info\n");
	return;
}

static int
check_arg_flag (char *arg)
{
	if (!arg || (*arg++ != '-')) {
		printf ("missing: - in arg 1 \n");
		return INVALID_ARG;
	}

	switch (*arg) {
		default : return INVALID_ARG;
		case 'l': return APP_LAUNCHING;
		case 'd': return DIR_SELECTION;
		case 'w': return WS_SWITCHING;
		case 's': return SCREEN_SAVER;
	}
}
	

static int
get_other_args (int to_benchmark, int argc, char *argv[])
{
	char tmp[256];

	if (!argv[2]) {
		printf ("missing: arg 2 \n");
		return FAILURE;
	}

	switch (to_benchmark) {
		case APP_LAUNCHING: 
		if (*argv[2] != '/') 
			strcat (app_full_path, argv[2]);
		else
			strcpy (app_full_path, argv[2]);

		/* this is to tell spytime to do tests that are
		 * exclusive for gnome-terminal - iconize, resize 
		 */
		strcpy (tmp, argv[2]);
		if (strstr (tmp, "gnome-terminal"))
			putenv ("APP_IS_TERMINAL=1");
		break;

		case DIR_SELECTION:
		strcat (app_full_path, "nautilus");
		sprintf (env_dirname, "SEL_DIR=%s", argv[2]);
		printf ("Benchmarking for dir [%s] selection \n", argv[2]);
		putenv (env_dirname);
		break;

		case WS_SWITCHING:
		putenv ("WS_SWITCH=1");
		sprintf (app_full_path, "%s/test-pager", argv[2]); 
		break;

		default:
		return FAILURE;
	}

	if (argv[3]) {
		sprintf (env_timer, "PBENCH_TIMER=%d", 1000 * atoi(argv[3])); 
		putenv (env_timer);
	}
	return SUCCESS;
}

static void 
setup_outfile (char arg)
{
	char *outdir = getenv ("OUTPUT_DIR");
	char **split_strings;
	char *app_name = NULL;
	char *tmp_str = NULL;
	int i;

	if (!outdir) {
		printf ("INFO: output directory not set. Setting to /tmp \n");
		outdir = "/tmp/";
	}
		
	/* get app name that's run */
	tmp_str = g_strdup (app_full_path);
	split_strings = g_strsplit (tmp_str, "/", -1);

	for (i = 0; split_strings[i]; ++i) 
		app_name = split_strings[i];

	/* g_return_if_fail (app_name && tmp_str);*/
	
	printf ("setup_outfile: appname [%s] \n", app_name);
	sprintf (env_outfile, "SPY_OUTFILE=%s/%s.%s.out", outdir, app_name, whats_tested[(int)arg]);
	printf ("setup_outfile: [%s] \n", env_outfile); 
	putenv (env_outfile);
	sprintf (env_appname, "APP_TO_TEST=%s", app_name); 
	putenv (env_appname);
}

static void 
setup_date (struct timeval t)
{
	char date_str[256];

	time_t t_sec = time (NULL);

	strcpy (date_str, ctime (&t_sec));
	date_str[strlen(date_str) - 1] = '\0';
	printf ("setup_date: %s \n", date_str);

	sprintf (env_date, "TIME_STAMP=%s", date_str);
	putenv (env_date);

	return;
}

/*
 * app_timer.c: measure the time for an app to start 
 */
int main (int argc, char *argv[])
{
	char to_benchmark;
	char *env_path;
	int pid;

	if (!argv[1]) {
		print_usage ();
		exit (1);
	}

	env_path = getenv ("PBENCH_PATH");
	if (env_path)
		strcpy (app_full_path, "env_path");
	else 
		printf ("PBENCH_PATH not set. Using %s instead\n", app_full_path);

	to_benchmark = check_arg_flag (argv[1]);
	if (to_benchmark == INVALID_ARG) {
		printf ("failure: error while parsing args \n");
		exit (1);
	}
	else if (to_benchmark == SCREEN_SAVER) {
		printf ("option not yet supported \n");
		exit (1);
	}

	if (get_other_args (to_benchmark, argc, argv) != SUCCESS) {
		printf ("Errors encountered while parsing args \n");
		exit (1);
	}

	printf ("benchmarking %s \n", app_full_path);

	if ( (pid = fork()) < 0) {
		perror ("fork:");
		exit (2);
	}

	if (!pid) {
		char env_starttime_sec[256] = {'\0'};
		char env_starttime_usec[256] = {'\0'};

		putenv ("GTK_MODULES=spytime:gail");

		setup_date (starttime);
		setup_outfile (to_benchmark); 

		gettimeofday (&starttime, &tzp);

		sprintf (env_starttime_sec, "START_TIME_SEC=%ld", starttime.tv_sec);
		sprintf (env_starttime_usec, "START_TIME_USEC=%ld", starttime.tv_usec);
		putenv (env_starttime_sec); putenv (env_starttime_usec);

		execl (app_full_path, app_full_path, (char *) 0);

		perror ("exec:");
		exit (127);
	}
	exit (0);
}
	
