/* Performance benchmarking tool
 * Copyright (C) 2002 Sun Microsystems, Inc.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */



#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/time.h>
#include <gtk/gtk.h>

#define DIRSEL_USE_TIMEOUT 1


#define EMISSION_HOOK(x,y,z)    g_signal_add_emission_hook \
                                (g_signal_lookup (x, GTK_TYPE_WIDGET), \
                                0, y, (gpointer *)z, (GDestroyNotify) NULL);

#define SIG_CONNECT(x, y, z, d) g_signal_connect(G_OBJECT(x), y, \
                                GTK_SIGNAL_FUNC(z), (gpointer)d);

#define REMOVE_HOOK(x,y)        g_signal_remove_emission_hook \
                                (g_signal_lookup (x, GTK_TYPE_WIDGET), y);

#define SIG_DISCONNECT(x,y)     g_signal_handler_disconnect \
                                (G_OBJECT(x), y);

enum {ICONIFY, DEICONIFY, MAXIMIZE, MINIMIZE, MOVE, RESIZE};

enum {CONFIG, FOCUS, EVENT, REALIZE, KEYPRESS, WIN_STATE_CHG, END_EVENT_LIST};

enum {FIRST_SWITCH, SECOND_SWITCH};

enum {NAUTILUS_TEST, LAUNCH_TEST, TERM_TEST, DIR_TEST, WS_TEST};

static     void           iconify_test (void);
static     void           deiconify_test (void);
static     void           maximize_test (void);
static     void           minimize_test (void);
static     void           move_test (void);
static     void           resize_test (void);
static     void           file_mgr_dir_sel_test (void);
static     void           ws_switch_test (void);
static     void           write_to_file (int, void *);
static     void           print_banner (FILE *, int);
static     gulong         get_timer_val (void);
static     gdouble        convert_time (gdouble, gdouble);
static     gboolean       win_state_changed (GtkWidget *, GdkEvent *, gpointer);
static     gboolean       timed_out (gpointer);
static     gboolean       dir_timed_out (gpointer);
static     gboolean       poll_for_last_file (gpointer);
static     gboolean       widget_realized (GSignalInvocationHint *, 
                          guint, const GValue *, gpointer);
static     gboolean       snoop_for_widget (GSignalInvocationHint *, 
                          guint, const GValue *, gpointer);
static     gboolean       dir_widget_realized (GSignalInvocationHint *, 
                          guint, const GValue *, gpointer);
static     gboolean       ws_switched (GSignalInvocationHint *, 
                          guint, const GValue *, gpointer);
static     GtkWindow *    get_top_level_window (void); 


typedef struct _for_ws_switch_test {
    AtkObject *pager_aobj;
    gint orig_ws;
    gint to_switch;
} WS_Data;
    
typedef struct _event_map {
    int i;
    char *name;
} event_map;

event_map event_list[] = { 
    {CONFIG, "Configure"}, 
    {FOCUS, "Focus"}, 
    {EVENT, "Event"}, 
    {REALIZE, "Realize"}, 
    {KEYPRESS, "key-press"}, 
    {WIN_STATE_CHG, "window-state-changed"}, 
    {END_EVENT_LIST, NULL},
};


static char *sel_dir = NULL;  /* indicate whether Nautilus dir selection test */
static char *ws_switch = NULL;  /* indicate workspace switch test */
static gulong hook_id;
static gulong timer_id;
static gulong sig_id;
static struct timeval starttime_icon, endtime_icon;
static struct timeval starttime_realize, endtime_realize;
static struct timeval endtime_realize_just_file;
static struct timezone tzp;
static GtkWindow *top_window;
static GtkWidget *top_widget;
static GtkWidget *icon_container_w[2]; 
static gint right_container = -1;
static GtkWidget *pager_w = NULL;
static gint root_x, root_y;  
static gint n_files_in_dir = 0;

/*
 * Performance benchmarking module for the following:
 * Time for app launch, iconfy, deiconify, maximize, minimize
 * FIXME: pass starttime as args and avoid using file
 */
int
gtk_module_init (int argc, char *argv[])
{
    gulong timer_val;

    /* char * timestamp = getenv ("TIME_STAMP");
    printf ("GOT TIME STAMP: %s \n", timestamp); */

    /* this will be set for -d option of pbench tool */
    sel_dir = getenv ("SEL_DIR");

    /* this will be set for -w option of pbench tool */
    ws_switch = getenv ("WS_SWITCH");

    /* We cannot be sure that the classes exist so we make sure that they do */
    gtk_type_class (GTK_TYPE_WIDGET);

    if (sel_dir)
    {
        printf ("seldir is %s \n", sel_dir);
        hook_id = EMISSION_HOOK("realize", snoop_for_widget, "FMIconContainer");
    }
    else if (ws_switch)
    {
        hook_id = EMISSION_HOOK("realize", snoop_for_widget, "WnckPager"); 
    }
    else
    {
        hook_id = EMISSION_HOOK("realize", widget_realized, REALIZE); 
    }

    /* to figure out a better way of detecting the last widget realized */
    timer_val = get_timer_val ();
    printf ("Setting timer to %ld\n", timer_val);

    g_timeout_add (timer_val, timed_out, NULL); 

    return TRUE;
}    


/* 
 * PBENCH_TIMER is set as a result of cmd line input for the parent 
 */
static gulong
get_timer_val ()
{
    char *str = getenv ("PBENCH_TIMER");
    return ((str) ? atoi(str) : (30 * 1000));
}

/* 
 * I could have hooked this into widget_realized but avoided 
 * doing so because it would mean addition processing and time - 
 * Not only would widget_realized() record the time-stamp but 
 * also end up poking into the widget details! 
 */

static gboolean 
snoop_for_widget (    GSignalInvocationHint *ihint,
                    guint                  n_param_values,
                    const GValue          *param_values,
                    gpointer               data)
{
    char *snoop_w = (char *)data;
    const char *w_name = NULL;
    GtkWidget *w;

    GtkWindow *window = NULL;
    /* printf ("."); fflush (stdout); */

    w = g_value_get_object (param_values + 0);
    w_name = gtk_widget_get_name (w);

    if (w_name && (strcmp (w_name, snoop_w) == 0))
    {
        printf ("Got FMIconContainer! \n");
        if  (!icon_container_w[0])
            icon_container_w[0] = pager_w = w;
        else if (!icon_container_w[1])
            icon_container_w[1] = pager_w = w;
        else
            printf ("SERIOUS: Hey, am receiving too many icon_containers!\n");
#if 0
        if (!strcmp (w_name, "FMIconContainer"))
        {
            printf ("Got FMIconContainer! \n");
            if (window = get_top_level_window())
            {
                printf ("Win title: %s \n", gtk_window_get_title (window));
                icon_container_w = w;
            }
            else
            {
                printf ("\t.. but belongs to .gnome_desktop :-( \n");
            }
        }
        else
        {
            pager_w = w;
        }
#endif
    }

    return TRUE;
}

static gboolean 
widget_realized(GSignalInvocationHint *ihint,
                guint                  n_param_values,
                const GValue          *param_values,
                gpointer               data)
{
    gettimeofday (&endtime_realize, &tzp);
    return TRUE;
}
    

static gboolean 
timed_out (gpointer data)
{
    static int first_time = 1;

    printf ("Inside timed_out \n"); 

    /* we need this timer only once */
    if (first_time)
        --first_time;
    else
        return FALSE;

    /* would result in chaos if widgets realize gets handled after timeout! */
    REMOVE_HOOK ("realize", hook_id);
    
    if (sel_dir)
    {
        file_mgr_dir_sel_test ();
    }
    else if (ws_switch)
    {
        ws_switch_test ();
    }
    else 
    {
        char *app_name;
        glong starttime_sec = atol (getenv ("START_TIME_SEC"));
        glong starttime_usec = atol (getenv ("START_TIME_USEC"));
        gdouble diff_secs;
        /* 
        printf ("child: started at %ld.%ld \n", starttime_sec, starttime_usec);
        printf ("child: realized all at %ld.%ld \n", 
                endtime_realize.tv_sec, endtime_realize.tv_usec);
        */
        diff_secs = 
            convert_time (endtime_realize.tv_sec, endtime_realize.tv_usec) - 
            convert_time (starttime_sec, starttime_usec) ;
        printf ("App realized in %f seconds \n", diff_secs);
        
        app_name = getenv ("APP_TO_TEST");
        printf ("app under test %s \n", app_name);
        if (strcmp (app_name, "nautilus") == 0)
        {
            write_to_file (NAUTILUS_TEST, &diff_secs);
            iconify_test ();
        }
        else if (strcmp (app_name, "gnome-terminal") == 0)
        {
            write_to_file (TERM_TEST, &diff_secs);
            maximize_test ();
        }
        else
        {
            write_to_file (LAUNCH_TEST, &diff_secs);
            exit (0);
        }
    }
    return FALSE;
}

            
static void
iconify_test ()
{
    printf ("Inside iconify_test \n");
    top_window = get_top_level_window ();
    g_return_if_fail (top_window);

    gtk_window_get_position (top_window, &root_x, &root_y); 
    /* printf ("iconify: x, y : %d, %d \n", root_x, root_y); */

    top_widget = (GtkWidget *)top_window;

    sig_id = SIG_CONNECT 
        (top_widget, "window-state-event", win_state_changed, ICONIFY);

    gettimeofday (&starttime_icon, &tzp);

    printf ("iconify: abt to iconify \n"); 

    gtk_window_iconify (top_window);

    return; 
}



static GtkWindow *
get_top_level_window ()
{
    const char *title;
    GList *win_list;
    GtkWindow *window;

    win_list = gtk_window_list_toplevels ();
    while (win_list)
    {
        window = win_list->data;

        title = gtk_window_get_title (window);
        
        if (!window || GTK_WIDGET (window)->parent || 
            GTK_IS_PLUG (window) || !title || !strcmp (title, ".gnome-desktop"))
        {
            printf ("get_top_level_window: window-title [%s] \n", title);
            /* FIXME: free me - needed ? */
        }
        else    
            return (window);

        win_list = win_list->next;
    }

    g_message ("No Match found");

    return (NULL);
}
    
static gboolean
win_state_changed (GtkWidget *w, GdkEvent *e, gpointer data)
{
    static char res_str[256] = {'\0'}; 
    static char *str[] = {    "iconify", "deiconify", "maximize", 
                            "minimize", "move", "resize"};

    char  tmp_str[256] = {'\0'};
    char  whats_tested;
    int i = (int)data;
    gdouble diff_secs;

    SIG_DISCONNECT (top_window, sig_id);
    
    gettimeofday (&endtime_icon, &tzp);

    g_return_val_if_fail ((i == ICONIFY || i == DEICONIFY || i == MAXIMIZE || i == MINIMIZE || i == MOVE || i== RESIZE ), TRUE);

    diff_secs= convert_time (endtime_icon.tv_sec, endtime_icon.tv_usec) - convert_time (starttime_icon.tv_sec, starttime_icon.tv_usec) ;

    printf ("Time taken for %s :  %f\n", str[i], diff_secs);
    sprintf (tmp_str, " -*- %f", diff_secs);
    strcat (res_str, tmp_str);
    
    if (i == ICONIFY)
    {
        deiconify_test ();
        return TRUE;
    }
    else if (i == DEICONIFY)
    {
        printf ("Nautilus tests complete \n");
        whats_tested = NAUTILUS_TEST;
    }
    else if (i == MAXIMIZE)
    {
        minimize_test (); 
        return TRUE;
    }
    else if (i == MINIMIZE)
    {
        move_test (); 
        return TRUE;
    }
    else if (i == MOVE)
    {
        resize_test (); 
        return TRUE;
    }
    else if (i == RESIZE)
    {
        printf ("Terminal tests complete \n");
        whats_tested = TERM_TEST;
    }
    strcat (res_str, "\n");
    write_to_file (whats_tested, res_str);
    exit(0);

}


static void
deiconify_test ()
{
    sig_id = SIG_CONNECT (top_widget, "window-state-event", win_state_changed, DEICONIFY);

    gettimeofday (&starttime_icon, &tzp);

    gtk_window_deiconify (top_window);

    return; 
}



static void
maximize_test ()
{
    top_window = get_top_level_window ();
    g_return_if_fail (top_window);

    gtk_window_get_position (top_window, &root_x, &root_y); 
    /* printf ("iconify: x, y : %d, %d \n", root_x, root_y); */

    top_widget = (GtkWidget *)top_window;

    sig_id = SIG_CONNECT 
        (top_widget, "configure-event", win_state_changed, MAXIMIZE);

    gettimeofday (&starttime_icon, &tzp);

    gtk_window_maximize (top_window);

    return; 
}



static void
minimize_test ()
{
    sig_id = SIG_CONNECT 
        (top_widget, "configure-event", win_state_changed, MINIMIZE);

    gettimeofday (&starttime_icon, &tzp);

    gtk_window_unmaximize (top_window);

    return; 
}

static void
move_test ()
{
    int gravity;
    gint win_width, win_height;

    /*printf ("move_test: initial position: (%u, %u) \n", root_x, root_y);*/

    gtk_window_get_size (top_window, &win_width, &win_height);

    /*printf ("move_test: size: (%u, %u) \n", win_width, win_height);*/

    gravity = gtk_window_get_gravity (top_window);
    if (gravity != GDK_GRAVITY_SOUTH_EAST)
        gtk_window_set_gravity (top_window, GDK_GRAVITY_SOUTH_EAST);

    sig_id = SIG_CONNECT
        (top_widget,"configure-event", win_state_changed, MOVE);

    /* default gravity is north west */
    gettimeofday (&starttime_icon, &tzp);

    gtk_window_move (top_window, gdk_screen_width() - 
        win_width, gdk_screen_height() - win_height);

    return;
}

static void
resize_test ()
{
    int gravity;
    gint win_width, win_height;

    gravity = gtk_window_get_gravity (top_window); 
    if (gravity != GDK_GRAVITY_NORTH_WEST)
        gtk_window_set_gravity (top_window, GDK_GRAVITY_NORTH_WEST);

    gtk_window_move (top_window, root_x, root_y ); /* NEW */

    /*printf ("move_test: initial position: (%d, %d) \n", root_x, root_y);*/

    gtk_window_get_size (top_window, &win_width, &win_height);

    /*printf ("move_test: size: (%d, %d) \n", win_width, win_height);*/

    sig_id = SIG_CONNECT 
        (top_widget,"configure-event", win_state_changed, RESIZE);

    /* default gravity is north west */
    gettimeofday (&starttime_icon, &tzp);

    gtk_window_resize (top_window, gdk_screen_width(), gdk_screen_height());

    return;

}

static gdouble 
convert_time (gdouble secs, gdouble microsecs)
{
    gdouble ms;

    ms = (gdouble)microsecs/1000000;
    ms += (gdouble)secs;

    return ms;
}

/*
 * click the directory using GAIL-exported action interface. 
 * Also get the number of files in the directory to be used 
 * by the callback function, dir_widget_realized
 */
static void
file_mgr_dir_sel_test ()
{
    gint i, j, found = 0;
    gint n_children;
    AtkObject *aobj, *child_obj;
    G_CONST_RETURN gchar *name = NULL;
    gchar *home_dir = NULL;
    gchar sel_dir_full_path[1024] = {'\0'};
    GError *dir_error = NULL;
    GDir *g_dir = NULL;

    /* am receiving FMIconContainer twice - apparently, one for
     * .gnome_desktop and one for Nautilus. Find which one is!
     */ 
    for (j = found = 0; !found && (j < 2); j++)
    {
        aobj = gtk_widget_get_accessible (icon_container_w[j]);
    
        n_children = atk_object_get_n_accessible_children (aobj);
        printf ("No. of files in current dir (n_children): %d \n", n_children); 
    
        g_return_if_fail (sel_dir); 
        printf ("Dir to select/click [%s] \n", sel_dir);
    
        for (i = 0; i < n_children; ++i)
        {
            child_obj = atk_object_ref_accessible_child (aobj, i);
            name = atk_object_get_name (child_obj);
            found = (name && !strcmp (sel_dir, name));
            if (found)
            {
                right_container = j;
                break;
            }
        }
    }


    if (!found)
    {
        printf ("failure: couldn't find the directory ? \n");
        exit (1);
    }

    if (ATK_IS_SELECTION (aobj))
        atk_selection_add_selection (ATK_SELECTION(aobj), i);
    else
        printf ("failure: Selection not supported ?? \n");

    /* find the number of files in the directory */

    printf ("Getting no. of files in [%s] \n", sel_dir);
    home_dir = getenv ("HOME");
    sprintf (sel_dir_full_path, "%s/%s", home_dir, sel_dir);

    g_dir = g_dir_open (sel_dir_full_path, 0, &dir_error);
    g_assert (g_dir);
        
    while (g_dir_read_name (g_dir))
        ++n_files_in_dir;

    printf ("Found [%d] files in [%s]\n", n_files_in_dir, sel_dir_full_path);
    g_dir_close (g_dir);

    if (!ATK_IS_ACTION (aobj))
        return;

    /* Current mechanism is to hook for widget realize and record their
     * timestamp. Find difference between the last recorded time stamp 
     * and start time stamp (recorded below)
     *     The alternative is to poll every second whether the last file
     * in the dir could be selected.
     */     
    
#ifdef DIRSEL_USE_TIMEOUT

    timer_id = g_timeout_add (1000, poll_for_last_file, NULL); 

#else

    hook_id = EMISSION_HOOK ("realize", dir_widget_realized, REALIZE); 
    /* FIXME: timeout may not be needed in future */
    g_timeout_add (60000, dir_timed_out, NULL); 

#endif
            

    gettimeofday (&starttime_realize, &tzp);
    atk_action_do_action (ATK_ACTION(aobj), 0);
}

static gboolean 
poll_for_last_file (gpointer data)
{
    gint n_children;
    AtkObject *aobj, *child_obj;
    const char *last_file_name = NULL;
	gdouble diff;

    aobj = gtk_widget_get_accessible (icon_container_w[right_container]);
    g_assert (aobj);

    n_children = atk_object_get_n_accessible_children (aobj);

    if (!n_children || (n_children != n_files_in_dir) || !(ATK_IS_SELECTION (aobj)))
    {
        printf ("Either --dir has no children-- or " 
                "--dir not realized-- or --selection not implemented--\n");
        return TRUE;
    }
    
	printf ("Trying to select file icon %d \n", n_children - 1);
    if (atk_selection_add_selection (ATK_SELECTION(aobj), n_children - 1) != TRUE)
    {
        printf ("Still unable to select the last file\n");
        return TRUE;
    }

    child_obj = atk_object_ref_accessible_child (aobj, n_children - 1);
    if (!child_obj)
    {
        printf ("Able to select but unable to ref child? \n");
        return TRUE;
    }

    gettimeofday (&endtime_realize_just_file, &tzp);
    g_source_remove (timer_id);

    last_file_name = atk_object_get_name (child_obj);
    printf ("Success! able to select the last file %s\n", last_file_name);

    diff = convert_time (endtime_realize_just_file.tv_sec, endtime_realize_just_file.tv_usec) - 
        convert_time (starttime_realize.tv_sec, starttime_realize.tv_usec);

    printf ("dir had %d files and all widgets realized in %f seconds \n", 
            n_files_in_dir, diff);

    write_to_file (DIR_TEST, &diff);

    exit (1);
}

static gboolean 
dir_widget_realized(GSignalInvocationHint *ihint,
                    guint                  n_param_values,
                    const GValue          *param_values,
                    gpointer               data)
{
    static gint done = 0;
    static AtkObject *aobj; 
    AtkObject *child_obj = NULL;
    gint n_children;
    const char *w_name = NULL;
    const char *last_file_name = NULL;
    GtkWidget *w;

    gettimeofday (&endtime_realize, &tzp);

    if (!aobj)
        aobj = gtk_widget_get_accessible (icon_container_w[right_container]);
    if (aobj)
    {
        n_children = atk_object_get_n_accessible_children (aobj);

        if (n_children && (n_children == n_files_in_dir) && (ATK_IS_SELECTION (aobj)))
        {
            if (!done && atk_selection_add_selection (ATK_SELECTION(aobj), n_children));
            {
                child_obj = atk_object_ref_accessible_child (aobj, n_children);
                if (child_obj)
                {
                    last_file_name = atk_object_get_name (child_obj);
                    printf ("Success! able to select the last file %s\n",
                        last_file_name);
                    gettimeofday (&endtime_realize_just_file, &tzp);
                    /* FIXME: should I stop with this emission hook here? */
                    done = 1;
                }
            }
        }
        else
        {
            printf ("Either --dir has no children-- or " 
                    "--dir not realized-- or --selection not implemented--\n");
        }

#if 0
        printf ("n_children while realizing: %d \n", n_children);
        child_obj = atk_object_ref_accessible_child (aobj, n_files_in_dir);
        if (child_obj)
        {
            gettimeofday (&endtime_realize, &tzp);
            printf ("\n");
        }
        else
        {
            printf ("."); fflush (stdout);
        }
#endif
        
    }
    
    return TRUE;
}
    
static gboolean 
dir_timed_out (gpointer data)
{
    gdouble diff;
    gint n_children;
    AtkObject *aobj, *child_obj;
    G_CONST_RETURN gchar *name = NULL; 

    aobj = gtk_widget_get_accessible (icon_container_w[right_container]);
    n_children = atk_object_get_n_accessible_children (aobj);

    printf ("n_children: %d \n", n_children);

    /* would result in chaos if widgets realize gets handled after timeout! */
    REMOVE_HOOK ("realize", hook_id);
    
    diff = convert_time (endtime_realize.tv_sec, endtime_realize.tv_usec) - 
        convert_time (starttime_realize.tv_sec, starttime_realize.tv_usec);

    printf ("dir had %d files and all widgets realized in %f seconds \n", 
            n_files_in_dir, diff);

    write_to_file (DIR_TEST, &diff);

    diff = convert_time (endtime_realize_just_file.tv_sec, endtime_realize_just_file.tv_usec) - convert_time (starttime_realize.tv_sec, starttime_realize.tv_usec);
    printf ("dir had %d files and only files realized in %f seconds \n", n_files_in_dir, diff);

    write_to_file (DIR_TEST, &diff);
    exit (0);
}


/* 
 * use the pager_w (obtained during realize event) to get its' 
 * atk object. perform atk selection on a wspace that is not
 * the currently selected one.
 * the control would flow to emission_hook after this
 */
static void
ws_switch_test ()
{
    gint n_wspaces, i; 
    WS_Data *data;
    
    if (!pager_w)
    {
        printf ("failure: did not catch pager_w \n");
        exit (1);
    }

    data = (WS_Data *) g_malloc (sizeof (*data));
    if (!data)
    {
        printf ("failure: malloc of WS_Data! \n");
        exit (1);
    }

    data->pager_aobj = gtk_widget_get_accessible (pager_w);
    if (!data->pager_aobj)
    {
        printf ("failure: did not get aobj for pager_w \n");
        exit (1);
    }

    n_wspaces = atk_object_get_n_accessible_children (data->pager_aobj);
    if (n_wspaces <= 1) 
    {
        printf ("Exiting - number of workspace = [%d] \n", n_wspaces);
        exit (0);
    }

    /* get the currently selected ws */
    for (i = 0; i < n_wspaces; ++i)
        if (atk_selection_is_child_selected(ATK_SELECTION(data->pager_aobj), i))
            break;

    if (i == n_wspaces)
    {
        printf ("failure: none of the workspaces selected? \n");
        exit (1);
    }
    data->orig_ws = i;
    
    /* default to 1st ws if it is not selected; otherwise last ws */
    data->to_switch = ((data->orig_ws) ? 0 : (n_wspaces - 1));

    hook_id = EMISSION_HOOK ("event", ws_switched, data);
 
    gettimeofday (&starttime_realize, &tzp);

    if (ATK_IS_SELECTION (data->pager_aobj))
        atk_selection_add_selection
            (ATK_SELECTION(data->pager_aobj), data->to_switch);
    else
        printf ("failure: selection interface not implemented ? \n");
}

/* 
 * FIXME: the pager widget doesn't emit a event that will
 * let me know that the ws has been switched. So I snoop
 * on the generic event class "event" and using atk object
 * check whether the switching has happened. figure out a
 * better method provided the widget emits a reliable event.
 *
 * perform atk selection to switch back to the initial wspace
 */
static gboolean 
ws_switched (    GSignalInvocationHint *ihint,
                guint                  n_param_values,
                const GValue          *param_values,
                gpointer               data)
{
    static int once_switched = 0;
    WS_Data *d = (WS_Data *)data;
    gdouble diff;

    if (atk_selection_is_child_selected
            (ATK_SELECTION(d->pager_aobj), d->to_switch))
    {
        printf ("switched once \n");
        once_switched = 1;
        atk_selection_add_selection (ATK_SELECTION(d->pager_aobj), d->orig_ws);
        return TRUE;
    }
        
    if (!once_switched)
    {
        printf ("yet to switch \n");
        return TRUE;
    }
    
    printf ("switched back to original \n");
    gettimeofday (&endtime_realize, &tzp);
    diff = convert_time (endtime_realize.tv_sec, endtime_realize.tv_usec) - 
        convert_time (starttime_realize.tv_sec, starttime_realize.tv_usec);
    
    printf ("workspace switched in %f seconds \n", diff);
    
    write_to_file (WS_TEST, &diff);

    exit (0);
}


static void
write_to_file (int whats_tested, void *vptr)
{
    static int first_time = 1; /* only for gnome-terminal */
    char add_banner = 0;
    char *fname = NULL;
    char *timestamp = NULL;  /* 'date' command format, passed by parent app */
    FILE *fp = NULL;

    fname = getenv ("SPY_OUTFILE");
    perror ("getenv");
    printf ("fname: %s \n", fname);
    g_return_if_fail (fname);

    if (g_file_test (fname, G_FILE_TEST_EXISTS) != TRUE)
        add_banner = 1;

    fp = fopen (fname, "a");
    g_return_if_fail (fp);

    printf ("Writing to file %s \n", fname);

    timestamp = getenv ("TIME_STAMP");
    g_return_if_fail (timestamp);    

    if (add_banner)
        print_banner (fp, whats_tested);

    if ((whats_tested == TERM_TEST) || (whats_tested == NAUTILUS_TEST))
    {
        if (first_time)
        {
            --first_time;
            fprintf (fp, "%s -*- %f", timestamp, *((gdouble *)vptr));
        }
        else
            fprintf (fp, "%s", (char *)vptr);
    }
    else
        fprintf (fp, "%s -*- %f\n", timestamp, *((gdouble *)vptr));

    fclose (fp);
    printf ("File successfully written! \n");
    return;
}
    

static void
print_banner (FILE *fp, int i)
{
#if 0
    fprintf (fp, 
        "* ---------------------------------------------------------\n");
    fprintf (fp, "*     UNDERSTANDING THE OUTPUT FORMAT  \n");
    fprintf (fp, "* The output fields are separated (delimited) by -*-\n");
    fprintf (fp, "* The first field represent date and time of sampling, \n");
    fprintf (fp, "* followed by time taken (secs) for each activity as \n");
    if (i  == NAUTILUS_TEST)
        fprintf (fp, "* follows: Time to launch, Iconize and Restore \n");
    else
        fprintf (fp, 
            "* Maximize, Minimize, Moving the terminal and Resizing.\n");
    fprintf (fp, "* ---------------------------------------------------------\n");
#endif

    fprintf (fp, 
    "*--------------------------------------------------------------\n");

    if (i == NAUTILUS_TEST)
        fprintf (fp, "* Date & time of sampling  -*- Launch   "
                     "-*- Iconize  -*- Restore \n"); 
    else if (i == TERM_TEST)
        fprintf (fp, "* Date & time of sampling  -*- Launch   -*- Maximize -*- "
                     "Minimize -*- MoveTerm -*- Resize  \n"); 
    else
        fprintf (fp, "* Date & time of sampling  -*- Time \n"); 

    fprintf (fp, 
    "*--------------------------------------------------------------\n");

    return;
}
