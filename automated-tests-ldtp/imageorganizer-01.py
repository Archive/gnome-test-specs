from ldtp import *
from ldtputils import *

APPNAME = "gthumb"
CASEID  = "imageorganizer-01"

log (CASEID, "begin")

launchapp ('gthumb')
waittillguiexist ('Home')
wait (5)

if not guiexist ('Home'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	selectmenuitem ('Home', 'mnuFile;mnuClose')
	wait (2)
	
	if guiexist ('Home'):
		print "Error: GUI has not closed"
		log (APPNAME + " window did not close", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Window did not close")

except:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")