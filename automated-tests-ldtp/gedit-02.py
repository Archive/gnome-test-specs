from ldtp import *
from ldtputils import *

import random

number = random.randint(0,999)

APPNAME = "gedit"
CASEID  = "gedit-02"

log (CASEID, "begin")

launchapp ('gedit')
waittillguiexist ('*-gedit')
wait (5)

if not guiexist ('*-gedit'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	click ('frm*gedit', 'btnNew')
	wait(2)
	
	generatekeyevent ('This is a test file')
	selectmenuitem ('*-gedit', 'mnuFile;mnuSaveAs')
	wait(7)

	if not guiexist ('dlgSave*'):
		print "Error: Save Dialog did not open"
		log (APPNAME + " Save Dialog did not open", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Save Dialog did not open")

	deletetext ('dlgSave*', 'txtName')
	enterstring ('dlgSave*', 'txtName', 'LDTP-Gedit-Save-' + str(number) +'.txt')
	click ('dlgSave*', 'btnSave')

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
