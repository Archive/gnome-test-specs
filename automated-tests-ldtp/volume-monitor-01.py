from ldtp import *
from ldtputils import *

launchapp ('vumeter')
wait(2)

if not guiexist ('Volume*'):
	print "Error: vumeter did not start a GUI"
	exit()

generatekeyevent ('<alt><f4>')
wait(1)

if guiexist ('Volume*'):
	print "Error: vumeter did not close after Alt F4 key combination"
	exit()

print "Passed"
