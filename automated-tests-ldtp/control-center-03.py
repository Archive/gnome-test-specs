from ldtp import *
from ldtputils import *

APPNAME = "gnome-control-center"
CASEID  = "control-center-03"

log (CASEID, "begin")

launchapp ('gnome-control-center')
waittillguiexist ('Control*')
wait (5)

if not guiexist ('Control*'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	click ('ControlCenter', 'btnAssistiveTechnologyPreferences')
	wait (5)
	
	if not guiexist ('AssistiveTechnologyPreferences'):
		print "Error: Assistive Technology Preferences did not start from control center"
		log (APPNAME + " Assistive Technology Preferences did not start from control center", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Assistive Technology Preferences did not start from control center")

	click ('AssistiveTechnologyPreferences', 'btnClose')
	wait (2)
	generatekeyevent ('<alt><f4>')
	wait (2)

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

try:	
	launchapp ('gnome-at-properties &')
	wait (5)
	
	if not guiexist ('AssistiveTechnologyPreferences'):
		print "Error: Assistive Technology Preferences did not start from command line"
		log (APPNAME + " Assistive Technology Preferences did not start from command line", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Assistive Technology Preferences did not start from command line")
	
	click ('AssistivetechonologyPreferences', 'btnClose')

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
