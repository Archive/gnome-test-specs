from ldtp import *
from ldtputils import *

APPNAME = "gedit"
CASEID  = "gedit-01"

log (CASEID, "begin")

launchapp ('gedit')
waittillguiexist ('*-gedit')
wait(5)

if not guiexist ('*-gedit'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	selectmenuitem ('*-gedit', 'mnuFile;mnuQuit')
	wait(5)
	
	if guiexist ('*gedit'):
		print "Error: GUI has not closed"
		log (APPNAME + " window did not close", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Window did not close")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
