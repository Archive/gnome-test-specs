from ldtp import *
from ldtputils import *

APPNAME = "gnome-character-map"
CASEID  = "character-map-01"

log (CASEID, "begin")

launchapp ('gnome-character-map')
waittillguiexist ('CharacterMap')
wait (5)

if not guiexist ('CharacterMap'):
	print "Error: gnome-character-map has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	selectmenuitem ('CharacterMap', 'mnuFile;mnuQuit')
	wait (5)
	
	if guiexist ('CharacterMap'):
		print "Error: GUI has not closed"
		log (APPNAME + " window did not close", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Window did not close")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
