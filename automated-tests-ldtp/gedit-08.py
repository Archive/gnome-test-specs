from ldtp import *
from ldtputils import *

import random

APPNAME = "gedit"
CASEID  = "gedit-08"

log (CASEID, "begin")

number = random.randint(0,999)
filename = "ldtpAutomatedText-" + str(number) + ".txt"
text = 'This is text added to a gedit document before it was saved for an LDTP automated test    '
textAdd = 'This is text added to a gedit document after it was saved.'

launchapp ('gedit')
waittillguiexist ('*-gedit')
wait (5)

if not guiexist ('*-gedit'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")


try:
	generatekeyevent (text)
	wait (2)
	selectmenuitem ('*-gedit', 'mnuFile;mnuSave')
	wait (2)
	   
	deletetext ('dlgSave*', 'txtName')
	enterstring ('dlgSave*', 'txtName', filename)
	wait (2)
	click ('dlgSave*', 'btnSave')
	wait (2)
	
	generatekeyevent (textAdd)
	wait (2)
	selectmenuitem ('*-gedit', 'mnuFile;mnuRevert')
	wait (2)
	if not guiexist ('dlg*'):
		print "Error: Revert Dialog did not open"
		log (APPNAME + " revert dialog did not open", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Revert Dialog did not open")
	
	# click ('dlg*', 'btnRevert') - Did not work for some reason
	generatekeyevent ('<alt>r')
	wait (2)
	
	revertedtext = gettextvalue ('*-gedit', 'txt0')
	if not revertedtext == text:
		print "Error: Reverted text does not equal original text"
		log (APPNAME + " reverted text does not equal original text", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Reverted text does not equal original text")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")