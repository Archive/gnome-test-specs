from ldtp import *
from ldtputils import *

launchapp ('gnome-session-properties')
wait (5)

if not guiexist ('Sessions'):
	print "Error: Session Proterties did not start"
	exit ()

click ('Sessions', 'btnAdd')
wait (2)

if not guiexist ('NewStartupProgram'):
	print "Error: New Startup Program dialogue did not popup"
	exit ()

enterstring ('NewStartupProgram', 'txtName', 'Terminal')
wait (2)
enterstring ('NewStartupProgram', 'txtCommand', 'gnome-terminal')
wait (2)
enterstring ('NewStartupProgram', 'txtComment', 'This is the Gnome terminal. Remove this if the terminal starts at startup!')
wait (2)

click ('NewStartupProgram', 'btnOK')
wait (2)

if guiexist ('NewStartupProgram'):
	print "Error: Did not properly click OK button on New Startup Program popup"
	exit ()

click ('Sessions', 'btnClose')
wait (2)

if guiexist ('Sessions'):
	print "Error: Did not properly close Sessions window"
	exit ()

print "Success"
