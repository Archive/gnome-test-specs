from ldtp import *
from ldtputils import *

APPNAME = "gnome-control-center"
CASEID  = "control-center-08"

log (CASEID, "begin")

launchapp ('gnome-control-center')
waittillguiexist ('ControlCenter')
wait (5)

if not guiexist ('ControlCenter'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	click ('ControlCenter', 'btnAssistiveTechnologyPreferences')
	wait (2)
	
	if not guiexist ('AssistiveTechnologyPreferences'):
		print "Error: Assistive Technology Preferences did not open from Control Center"
		log (APPNAME + " Assistive Technology Preferences did not open from Control Center", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Assistive Technology Preferences did not open from Control Center")
	
	click ('AssistiveTechnologyPreferences', 'btnHelp')
	click ('AssistiveTechnologyPreferences', 'btnClose')
	wait (10)
	waittillguiexist ('AssistiveTechnologyPreferences')
	
	if not guiexist ('AssistiveTechnologyPreferences'):
		print "Error: Assistive Technology Preferences Help did not start from Assistive Technology Preferences"
		log (APPNAME + " Assistive Technology Preferences Help did not start from Assistive Technology Preferences", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Assistive Technology Preferences Help did not start from Assistive Technology Preferences")

	selectmenuitem ('AssistiveTechnologyPreferences', 'mnuFile;mnuClose')
	wait (5)
	
	if guiexist ('AssistiveTechnologyPreferences'):
		print "Error: A window with the title Assistive Technology Preferences is still open"
		log (APPNAME + " A window with the title Assistive Technology Preferences is still openes", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("A window with the title Assistive Technology Preferences is still open")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
