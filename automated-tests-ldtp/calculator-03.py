from ldtp import *
from ldtputils import *

APPNAME = "gnome-calculator"
CASEID  = "calculator-03"

log (CASEID, "begin")

launchapp ('gnome-calculator')
waittillguiexist ('*Calculator*')
wait (5)

if not guiexist ('*Calculator'):
	print "Error: gnome-calculator did not start"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	generatekeyevent ("<ctrl>b")
	
	# Addition
	generatekeyevent ("<ctrl>a<delete>")
	generatekeyevent ("5+5<enter>")
	addition = gettextvalue ('*Calculator*', 'txt0')
	wait (2)
	print "addition equals: " + addition
	additionInt = ""
	for each in addition:
		if each == ".":
			break
		additionInt = additionInt + each
	print "additionInt equals: " + additionInt
	if not additionInt == "10":
		print "Error: Addition did not equal what was expected"
		log (APPNAME + " addition did not equal expected", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Addition did not equal expected")
	
	# Subtraction
	generatekeyevent ("<ctrl>a<delete>")
	generatekeyevent ("10-5<enter>")
	subtraction = gettextvalue ('*Calculator*', 'txt0')
	wait (2)
	subtractionInt = ""
	for each in subtraction:
		if each == ".":
			break
		subtractionInt = subtractionInt + each
	if not subtractionInt == "5":
		print "Error: Subtraction did not equal what was expected"
		log (APPNAME + " subtraction did not equal expected", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Subtraction did not equal expected")
	
	# Multiplication
	generatekeyevent ("<ctrl>a<delete>")
	generatekeyevent ("5*5<enter>")
	multiplication = gettextvalue ('*Calculator*', 'txt0')
	wait (2)
	multiplicationInt = ""
	for each in multiplication:
		if each == ".":
			break
		multiplicationInt = multiplicationInt + each
	if not multiplicationInt == "25":
		print "Error: Multiplication did not equal what was expected"
		log (APPNAME + " multiplication did not equal expected", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Multiplication did not equal expected")
	 
	# Division
	generatekeyevent ("<ctrl>a<delete>")
	generatekeyevent ("25/5<enter>")
	division = gettextvalue ('*Calculator*', 'txt0')
	wait (2)
	divisionInt = ""
	for each in division:
		if each == ".":
			break
		divisionInt = divisionInt + each
	if not divisionInt == "5":
		print "Error: Division did not equal what was expected"
		log (APPNAME + " division did not equal expected", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Division did not equal expected")
	
	# Clear
	generatekeyevent ("<ctrl>a<delete>")
	generatekeyevent ("1234<enter>")
	click ('*Calculator*', 'btnClear1')
	clear = gettextvalue ('*Calculator*', 'txt0')
	wait (2)
	clearInt = ""
	for each in clear:
		if each == ".":
			break
		clearInt = clearInt + each
	if clearInt == "1234":
		print "Error: Clear did not equal what was expected"
		log (APPNAME + " clear did not equal expected", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("clear did not equal expected")
	
	# Backspace
	generatekeyevent ("<ctrl>a<delete>")
	generatekeyevent ("1234")
	click ('*Calculator*', 'btnBackspace')
	backspace = gettextvalue ('*Calculator*', 'txt0')
	wait (2)
	backspaceInt = ""
	# If this is required for backspace, it will cause an error in the test.
	for each in backspace:
		if each == ".":
			break
		backspaceInt = backspaceInt + each
	if not backspaceInt == "123":
		print "Error: Backspace did not equal what was expected"
		log (APPNAME + " backspace did not equal expected", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("backspace did not equal expected")
	
	# CE
	generatekeyevent ("<ctrl>a<delete>")
	generatekeyevent ("1234")
	click ('*Calculator*', 'btnClearentry1')
	ce = gettextvalue ('*Calculator*', 'txt0')
	wait (2)
	ceInt = ""
	for each in ce:
		if each == ".":
			break
		ceInt = ceInt + each
	if ceInt == "1234":
		print "Error: CE did not equal what was expected"
		log (APPNAME + " CE did not equal expected", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("CE did not equal expected")


except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

if guiexist ('gnome-calculator'):
	print "Error: GUI has not closed"
	exit ()

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
