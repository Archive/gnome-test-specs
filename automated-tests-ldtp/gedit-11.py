from ldtp import *
from ldtputils import *

APPNAME = "gedit"
CASEID  = "gedit-11"

log (CASEID, "begin")

before = "before "
after = "after"
both = before + after

launchapp ('gedit')
wait (5)

def runTest(version):
	try:
		generatekeyevent (before)
		generatekeyevent (after)
		wait (2)
		allin = gettextvalue ('*gedit', 'txt0')
		if not allin == both:
			print "Error: The entire string is not in the text document before undo, " + version
			log (APPNAME + " The entire string is not in the text document before undo, " + version, "cause")
			log (CASEID, "fail")
			raise LdtpExecutionError ("The entire string is not in the text document before undo, " + version)
		
		wait (2)
		if version == 'button':
			click ('*gedit', 'btnUndo')
		elif version == 'menu':
			selectmenuitem ('*gedit', 'mnuEdit;mnuUndo')
		elif version == 'shortcut':
			generatekeyevent ('<ctrl>z')
		
		wait (2)
		undone = gettextvalue ('*gedit', 'txt0')
		if not undone == before:
			print "Error: The string that should be output when undone is not correct, " + version
			log (APPNAME + " The string that should be output when undone is not correct, " + version, "cause")
			log (CASEID, "fail")
			raise LdtpExecutionError ("The string that should be output when undone is not correct, " + version)
		
		wait (2)
		if version == 'button':
			click ('*gedit', 'btnRedo')
		elif version == 'menu':
			selectmenuitem ('*gedit', 'mnuEdit;mnuRedo')
		elif version == 'shortcut':
			generatekeyevent ('<ctrl><shift>z')
		
		wait (2)
		redone = gettextvalue ('*gedit', 'txt0')
		if not redone == both:
			print "Error: The string that should be output when redone is not correct, " + version
			log (APPNAME + " The string that should be output when redone is not correct, " + version, "cause")
			log (CASEID, "fail")
			raise LdtpExecutionError ("The string that should be output when redone is not correct, " + version)
		
		wait(2)
		generatekeyevent ('<ctrl>a')
		generatekeyevent ('<delete>')
		wait (5)
	
	except LdtpExecutionError, msg:
		log (str (msg), "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError (str (msg))

runTest('button')
runTest('menu')
runTest('shortcut')

wait (2)

log (CASEID, "pass")
log (CASEID, "end")