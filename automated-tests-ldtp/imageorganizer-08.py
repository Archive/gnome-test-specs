from ldtp import *
from ldtputils import *

APPNAME = "gthumb"
CASEID  = "imageorganizer-08"

log (CASEID, "begin")

launchapp ('gthumb')
waittillguiexist ('Home')
wait (5)

selectmenuitem ('Home', 'mnuFile;mnuNewFolder')
wait (2)

generatekeyevent ('gThumbTestFolder')
click ('dlg*', 'btnCreate')
wait (2)

selectmenuitem ('Home', 'mnuFile;mnuNewFolder')
wait (2)

generatekeyevent ('gThumbTestFolder')
click ('dlg*', 'btnCreate')
wait (2)

if not guiexist ('dlg*'):
	print "Error: Error dialog did not start when trying to create 2 identical folder names"
	exit ()

click ('dlg*', 'btnClose')
wait (2)

selectmenuitem ('Home', 'mnuFile;mnuClose')
print "Success"
