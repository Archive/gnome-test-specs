from ldtp import *
from ldtputils import *

import os

launchapp ('gnome-panel-screenshot')
wait(2)

generatekeyevent ('LDTP-Screenshot')
wait(2)

click ('frm*Screenshot', 'btnSave')
wait(2)

if not os.path.exists ('/home/' + os.getenv('HOME') + '/Desktop/LDTP-Screenshot.png'):
	print "Error: Screenshot did NOT save to Desktop under filename LDTP-Screenshot!"
	print ""
	print "If the path to your Desktop is not in this format: /home/[user]/Desktop"
	print "or the default screenshot save location is not the desktop, disregard"
	print "this error message."

print "Success"
