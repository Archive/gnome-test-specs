from ldtp import *
from ldtputils import *

APPNAME = "gnome-calculator"
CASEID  = "calculator-13"

launchapp ('gnome-calculator')
waittillguiexist ('*Calculator*')
wait (5)

if not guiexist ('Calculator*'):
	print "Error: gnome-calculator has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	selectmenuitem ('Calculator*', 'mnuHelp;mnuAbout')
	wait (2)
	
	if not guiexist ('About*'):
		print "Error: About Calculator window did not show"
		log (APPNAME + " About window does not exist", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("About Window does not exist")
	
	click ('About*', 'btnClose')
	#selectmenuitem ('Calculator*', 'mnuCalculator;mnuQuit') does not work for some reason
	wait (2)
	generatekeyevent ('<alt><f4>')

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))
