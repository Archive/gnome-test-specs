from ldtp import *
from ldtputils import *

APPNAME = "gedit"
CASEID  = "gedit-14"

log (CASEID, "begin")

launchapp  ('gedit')
waittillguiexist ('*-gedit')
wait (5)

if not guiexist ('*-gedit'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	text = "This is some text for a LDTP automated test"
	generatekeyevent (text)
	wait (2)
	
	generatekeyevent ('<ctrl>a')
	wait (2)
	generatekeyevent ('<ctrl>x')
	wait (2)
	
	click ('*-gedit', 'btnNew')
	wait (2)
	generatekeyevent ('<ctrl>v')
	wait (2)
	
	textMatch = gettextvalue ('*gedit', 'txt1')
	
	if not textMatch == text:
		print "Error: Text in gedit was not pasted into the new document"
		exit ()
	
	textNotMatch = gettextvalue('*-gedit', 'txt0')
	if textNotMatch == text:
		print "Error: Text in gedit first document did not cut properly"
		exit ()

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
