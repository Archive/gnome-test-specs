from ldtp import  *
from ldtputils import *

launchapp ('gnome-session-properties')
wait (5)

if not guiexist ('Sessions')
	print "Error: gnome-session-properties did not start"
	exit ()

click ('Sessions', 'btnClose')
wait (2)

if guiexist ('Sessions')
	print "Error: gnome-session-properties did not close"
	exit ()

print "Success"
