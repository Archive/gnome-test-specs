from ldtp import *
from ldtputils import *

launchapp ('gnome-panel-screenshot')
waittillguiexist ('*Screenshot')
wait (5)

if not guiexist ('*Screenshot'):
	print "Error: Screenshot application did not start"
	exit ()

click ('*Screenshot', 'btnHelp')
waittillguiexist ('TakingScreenshots')
wait (5) # Help takes a long time to load, just giving it an extra 5 seconds just incase

if not guiexist ('frmTakingScreenshots'):
	print "Error: Screenshot help did not start from Help button within application"
	exit ()

generatekeyevent ('<alt><f4>')
click ('*Screenshot', 'btnCancel')
wait (5)

try:
	launchapp ('gnome-panel-screenshot --help')
except NameError:
	print "Error: Help from command line did not work with NameError exception"
	exit ()
except:
	print "Error: Help from command line did not work, unknown error"
	exit ()

print "Success"
