from ldtp import *
from ldtputils import *

APPNAME = "gnome-cd"
CASEID  = "cd-player-01"

launchapp ('gnome-cd')
waittillguiexist ('CDPlayer')
wait (5)

if not guiexist ('CDPlayer'):
	print "Error: gnome-cd did not start"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	generatekeyevent ("<alt><f4>")
	wait (5)
	
	if guiexist ('CDPlayer'):
		print "Error: gnome-cd did not close"
		log (APPNAME + " window did not close", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Window did not close")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
