from ldtp import  *
from ldtputils import *

# Note: This testing script will require human interaction
# to check that the recording is actually being monitored.

launchapp ('vumeter -r')
launchapp ('gnome-sound-recorder')
wait (5)

if not guiexist ('Recording*'):
	print "Error: Recording Monitor did not start"
	exit ()

if not guiexist ('*Recorder'):
	print "Error: Sound Recorder did not start"
	exit ()

print "LDTP will now start a recording in the Gnome Sound Recorder for 5 seconds"
print "You will need to see that the recording monitor actually monitors the recording"

wait (2)
click ('Untitled*', 'btnRecord')
wait (5)
click ('Untitled*', 'btnStop')

wait (3)
print "Success, unless Recording Monitor did not monitor recording"
