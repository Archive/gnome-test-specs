from ldtp import *
from ldtputils import *

APPNAME = "gedit"
CASEID  = "gedit-15"

log (CASEID, "begin")

launchapp  ('gedit')
waittillguiexist ('*-gedit')
wait (5)

if not guiexist ('*-gedit'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	text = "This is some text for a LDTP automated test"
	generatekeyevent (text)
	wait (2)
	
	textEntered = gettextvalue ('*-gedit', 'txt0')
	wait (2)
	
	generatekeyevent ('<ctrl>a')
	wait (2)
	
	selectmenuitem ('*-gedit', 'mnuEdit;mnuDelete')
	wait (2)
	
	textDeleted = gettextvalue ('*-gedit', 'txt0')
	if textEntered == textDeleted:
		print "Error: Text did not select all and/or delete"
		log (APPNAME + " text did not select and/or delete", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Text did not select all and/or delete")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")