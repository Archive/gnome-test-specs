from ldtp import *
from ldtputils import *

APPNAME = "gnome-control-center"
CASEID  = "control-center-01"

log (CASEID, "begin")

launchapp ('gnome-control-center')
waittillguiexist ('Control*')
wait(5)

if not guiexist ('Control*'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	generatekeyevent ('<alt><f4>')
	
	if guiexist ('frmControl*'):
		print "Error: GUI has not closed"
		log (APPNAME + " window did not close", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Window did not close")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg)

wait (2)

log (CASEID, "pass")
log (CASEID, "end")
