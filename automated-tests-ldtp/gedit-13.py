from ldtp import *
from ldtputils import *

APPNAME = "gedit"
CASEID  = "gedit-13"

log (CASEID, "begin")

launchapp  ('gedit')
waittillguiexist ('*-gedit')
wait (5)

if not guiexist ('*-gedit'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	text = "This is some text for a LDTP automated test"
	generatekeyevent (text);wait (2)
	generatekeyevent ('<ctrl>a');wait (2)
	generatekeyevent ('<ctrl>c');wait (2)
	click ('*-gedit', 'btnNew');wait (2)
	generatekeyevent ('<ctrl>v');wait (2)
	
	textMatch = gettextvalue('*-gedit', 'txt1')
	wait (2)
	if not textMatch == text:
		print "Error: Text in gedit did not copy to new document"
		log (APPNAME + " text in gedit did not copy to new document", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Text in gedit did not copy to new document")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")