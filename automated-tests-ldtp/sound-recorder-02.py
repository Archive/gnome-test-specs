from ldtp import *
from ldtputils import *

launchapp ('gnome-sound-recorder')
wait (5)

click ('*Recorder', 'btnNew')
wait (2)
click ('Untitled*', 'btnRecord')
wait (8)

click ('Untitled*', 'btnStop')
wait (2)
click ('Untitled*', 'btnPlay')
wait (10)

selectmenuitem ('Untitled*', 'mnuFile;mnuSaveAs')
