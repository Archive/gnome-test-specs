from ldtp import *
from ldtputils import *

APPNAME = "gnome-calculator"
CASEID  = "calculator-07"

launchapp ('gnome-calculator')
waittillguiexist ('*Calculator*')
wait (5)

if not guiexist ('*Calculator*'):
	print "Error: gnome-calculator has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	generatekeyevent ("<ctrl>q")
	wait (2)
	if guiexist ("*Calculator*"):
		print "Error: Calculator Did Not Close"
		log (APPNAME + " window did not close", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Window did not close")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))


launchapp ('gnome-calculator')
waittillguiexist ('*Calculator*')
wait (5)

if not guiexist ('*Calculator*'):
	print "Error: gnome-calculator has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	click ("frm*Calculator*", "btnNumeric4")
	click ("frm*Calculator*", "btnNumeric7")
	click ("frm*Calculator*", "btnNumeric2")
	wait (5)

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

try:
	mouseleftclick ("frm*Calculator*", "txt0")
	calculatorValue = gettextvalue ("frm*Calculator*", "txt0")
	generatekeyevent ("<ctrl>c")
	
	selectmenuitem ('*-gedit', 'mnuFile;mnuClose')
	wait (1)
	launchapp ('gedit')
	wait (3)
	generatekeyevent ("<ctrl>v")
	textValue = gettextvalue ('*gedit', 'txt0')
	wait (3)
	
	if not calculatorValue == textValue:
		print "Error: Did not copy and paste from calculator to text editor"
	
	selectmenuitem ('*Calculator*', 'mnuCalculator;mnuQuit')
	
	generatekeyevent ("<ctrl>n")
	wait (2)
	numbers = "42346"
	generatekeyevent (numbers)
	generatekeyevent ("<ctrl>a")
	wait (1)
	generatekeyevent ("<ctrl>c")
	wait (1)
	
	launchapp ('gnome-calculator')
	wait (1)
	click ('frm*Calculator*', 'btnClear')
	wait (2)
	mouseleftclick ("frm*Calculator*", "txt0")
	generatekeyevent ("<ctrl>v")
	calculatorValue = gettextvalue ("frm*Calculator*", "txt0")
	
	if not calculatorValue == numbers:
		print "Error: Did not copy and paste from text editor to calculator"
		log (APPNAME + " did not copy and paste from text editor to calculator", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Did not copy and paste from text editor to calculator")

	wait (2)
	selectmenuitem ('*Calculator*', 'mnuCalculator;mnuQuit')
	
	launchapp ('gnome-calculator')
	mouseleftclick ("frm*Calculator*", "txt0")
	generatekeyevent ("<ctrl>i")
	wait (1)
	generatekeyevent ("a")
	wait (1)
	click ("frm*Value*", "btnInsert")
	
	if not gettextvalue ("frm*Calculator*", "txt0") == "97":
		print "Error: Value did not return expected"
		log (APPNAME + " value did not return expected", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Value did not return expected")
	
	selectmenuitem ('*Calculator*', 'mnuCalculator;mnuQuit')
	launchapp ('gnome-calculator')
	
	wait (2)
	generatekeyevent ('<ctrl>b')
	wait (2)
	generatekeyevent ('<ctrl>a')
	wait (2)
	generatekeyevent ('<ctrl>f')
	wait (2)
	generatekeyevent ('<ctrl>s')
	
	wait (2)
	generatekeyevent ('<ctrl>t')
	generatekeyevent ('23864')
	generatekeyevent ('<enter>')
	
	wait (2)
	generatekeyevent ('<ctrl>k')
	generatekeyevent ('100000')
	generatekeyevent ('<enter>')
	
	wait (2)
	generatekeyevent ('<ctrl>m')
	
	wait (2)
	generatekeyevent ('<ctrl>r')
	
	wait (2)
	generatekeyevent ('<f1>')
	
	wait (2)
	generatekeyevent ('2453454')
	generatekeyevent ('<esc>')

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))
