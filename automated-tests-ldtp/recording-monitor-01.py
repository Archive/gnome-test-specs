from ldtp import *
from ldtputils import *

launchapp ('vumeter -r')
wait (5)

if not guiexist ('Recording*'):
	print "Error: Recording Monitor did not start"
	exit ()

generatekeyevent ('<alt><f4>')
wait (2)

if guiexist ('Recording*'):
	print "Error: Recording Monitor did not close"
	exit()

print "Success"
