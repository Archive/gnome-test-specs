from ldtp import *
from ldtputils import *

APPNAME = "gnome-about"
CASEID  = "about-02"

log (CASEID, "begin")

launchapp ('gnome-about')
waittillguiexist ('About*')
wait (5)

if not guiexist ('About*'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	click ('About*', 'btnClose')
	wait (2)
	
	if guiexist ('About*'):
		print "Error: GUI has not closed"
		log (APPNAME + " window did not close", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Window did not close")

except LdtpExecutionError, msg:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

wait (2)

log (CASEID, "pass")
log (CASEID, "end")