from ldtp import *
from ldtputils import *

APPNAME = "gthumb"
CASEID  = "imageorganizer-01"

log (CASEID, "begin")

launchapp ('gthumb')
wait (5)

if not guiexist ('Home'):
	print "Error: " + APPNAME +" has not opened"
	log (APPNAME + " window does not exist", "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError ("Window does not exist")

try:
	success = selectmenuitem ('Home', 'mnuHelp;mnuContents')
	wait (2)
	if success == 0:
		print "Error launching \"Help -> Contents\""
		log (APPNAME + " Error launching \"Help -> Contents\"", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Error launching \"Help -> Contents\"")
	
	wait (5)
	success = generatekeyevent ('<alt><f4>')
	wait (2)
	if success == 0:
		print "Error: Problem closing gThumb Help"
		log (APPNAME + " Problem closing gThumb Help", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Problem closing gThumb Help")
	wait (5)
	
	success = selectmenuitem ('Home', 'mnuHelp;mnuKeyboardShortcuts')
	wait (2)
	if success == 0:
		print "Error launching \"Help -> Keyboard Shortcuts\""
		log (APPNAME + " Error launching \"Help -> Keyboard Shortcuts\"", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Error launching \"Help -> Keyboard Shortcuts\"")
	wait (5)
	
	success = generatekeyevent ('<alt><f4>')
	wait (2)
	if success == 0:
		print "Error: Problem closing Keyboard Shortcuts Help"
		log (APPNAME + " Problem closing Keyboard Shortcuts Help", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Problem closing Keyboard Shortcuts Help")
	wait (5)
	
	success = selectmenuitem ('Home', 'mnuHelp;mnuAbout')
	wait (2)
	if success == 0:
		print "Error launching \"Help -> About\""
		log (APPNAME + " Error launching \"Help -> About\"", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Error launching \"Help -> About\"")
	wait (5)
	
	success = generatekeyevent ('<alt><f4>')
	wait (2)
	if success == 0:
		print "Error: Problem closing About page"
		log (APPNAME + " Problem closing About page", "cause")
		log (CASEID, "fail")
		raise LdtpExecutionError ("Problem closing About page")
	wait (5)

except:
	log (str (msg), "cause")
	log (CASEID, "fail")
	raise LdtpExecutionError (str (msg))

print "Success"
