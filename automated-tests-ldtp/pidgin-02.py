from ldtp import *
from ldtputils import *

import os

launchapp ('pidgin')
wait (5)
selectmenuitem ('BuddyList', 'mnuAccounts;mnuAdd/Edit')
wait (2)

if not guiexist ('frmAccounts'):
	print "Error: Accounts popup did not open"
	exit()

click ('frmAccounts', "btnAdd")
wait (2)

pro = [
'AIM',
'Bonjour',
'Gadu-Gadu',
'Google Talk',
'GroupWise',
'ICQ',
'IRC',
'MSN',
'MySpaceIM',
'QQ',
'SIMPLE',
'Sametime',
'XMPP',
'Yahoo',
'Zephyr']

errors = 0
os.popen ('rm comboboxitem.lst')

capturetofile ('frmAdd*', 'cboProtocol')
wait (2)
f = open('comboboxitem.lst', 'r')
text = f.readlines()
os.popen ('rm comboboxitem.lst')

for i in pro:
	a = 0
	for b in text:
		if b == i + "\n":
			a = 1
	if a == 0:
		print "Error: " + i + " is not in the list"
		errors = errors + 1
wait (2)

if not errors == 0:
	print "Error: There were " + str(errors) + " protocols not in the list"
	exit ()

print "Success"
