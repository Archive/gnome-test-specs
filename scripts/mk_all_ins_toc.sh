#!/bin/sh

base_dir="/scde/web/docs/test/gnome_test"
dir="$base_dir/install"
script="$base_dir/scripts/mk_toc_comp.pl"


cd $dir

cd install_tests ; $script install_tests install; chmod 644 *toc.html; cd ..
cd graphic       ; $script graphic   install    ; chmod 644 *toc.html; cd ..
cd ins_disk      ; $script ins_disk  install    ; chmod 644 *toc.html; cd ..
cd lang          ; $script lang      install     ; chmod 644 *toc.html; cd ..

