#!/usr/bin/nawk 
BEGIN{
FS="@@@"
Y=X=1 
}
{
if ($1 != "")                           		#ignore blank lines 
	{ 
	RESULT=$5 
	BUG=$6 
	if (MAJOR[X] != $2) { X++} 			# seperate each major test group 
	MAJOR[X]=$2					# set major test component to current
	if ( MINOR[X,Y] != $3 ) {Y++} 
	MINOR[X,Y]=$3 
	LINK[X,Y]=$1
	#print ">>>"MINOR[X,Y]"<<<"Y 
	#print ">"LINK[X,Y]"<"
	#print BUG
	if (RESULT == "N") { NCOUNT[X,Y]++} 
	if (RESULT == "P") { PCOUNT[X,Y]++}		
	if (RESULT == "F") { FCOUNT[X,Y]++}
	if (BUG    != "<TD><BR></TD></TR>" ) { BCOUNT[X,Y]++}  
	}
} 
END{
print "<HTML><HEAD><TITLE>Test Summary</TITLE></HEAD>"
print "<BODY BGCOLOR=ffffff>"
print "<P>"
print "<CENTER><TABLE BORDER=2 CELLPADDING=2 CELLSPACING=2>"
print "<CAPTION><B>Test Summary  "HEADING"</B></CAPTION>"
print "<TR><TD>Major Test</TD>"
print "<TD>Minor Test </TD>"
print "<TD>Number of tests passed</TD>"
print "<TD>Number of tests failed</TD>"
print "<TD>Number of tests not run</TD>"
print "<TD>Number of Bugs Logged</TD></TR>"

for ( x = 2 ; x <= X ; x++)
	{
	for ( y = 2 ; y <= Y ; y++)
		{
		if (MINOR[x,y] != "")
		printf "<TR><TD><P ALIGN=CENTER>%s</P></TD><TD><P ALIGN=CENTER><A HREF=\"%s\">%s</A></P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD><TD><P ALIGN=CENTER>%d</P></TD></TR>\n",MAJOR[x], LINK[x,y], MINOR[x,y], PCOUNT[x,y], FCOUNT[x,y], NCOUNT[x,y], BCOUNT[x,y] 
		}
	}
print "</CENTER></TABLE>" 
print "</P>"
print "<P ALIGN=CENTER>"
print "<A HREF=\"http://www-cde.ireland/test/gnome_test\"><B><FONT SIZE=5 STYLE=\"font-size: 20pt\">Gnome Test Suite</FONT>"
print "</B></A></P>"
print "</BODY></HTML>"
}

