#!/bin/sh

base_dir=".."
dir="$base_dir/interop"
script="../../scripts/make_index.pl"

cd $dir

cd text_widgets; $script interop text_widgets; cd ..

cd ..
