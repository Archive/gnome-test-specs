#!/bin/sh -x 

base_dir=".."
dir="$base_dir/install"
script="../../scripts/make_index.pl"

cd $dir

cd install_tests  ; $script install install_tests ; chmod 755 index.html ; cd ..
cd graphic ;     $script install graphic ; chmod 755 index.html ; cd ..
cd ins_disk   ; $script install ins_disk  ; chmod 755 index.html ; cd ..
cd lang     ; $script install  lang    ; chmod 755 index.html ; cd ..
