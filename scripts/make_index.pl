#!/usr/bin/perl
#
# make_index.pl
#
#   Makes an HTML file listing all the individual test
#   assertions found in the current directory.
#   The output file from this script will be:
#     ./index.html
#   Testers will use this page to record test results.
#
# 25-Jan-2001, dermot.mccluskey, shane.oconnor
#   initial version.
#

#
# Main routine
#
$PROG=`basename $0`;

if ( length( $ARGV[1] ) == 0 ) {
	print "Usage: $PROG MAJOR_TEST_ID MINOR_TEST_ID\n";
	exit 1;
}

# set up some global variables
$MAJOR_TEST_ID=$ARGV[0];
$MINOR_TEST_ID=$ARGV[1];
$SCRIPT_DIR="/scde/web/docs/test/gnome_test/scripts";
$INPUT_DIR=".";
$OUTPUT_DIR=".";
$HTML_FORM="${OUTPUT_DIR}/index.html";
$CFG_INPUT="${SCRIPT_DIR}/make_index.cfg";
$ACTION_URL="/cgi-bin/gnome_results.cgi";
#$ACTION_URL="/cgi-bin/gnome_bob.cgi";
$TOC="${MINOR_TEST_ID}_toc.html";
$DATE_TIME_STAMP=`/usr/bin/date +%C`;
$RECORD_BUTTON_LABEL="SAVE RESULTS";

if ( ! -d $OUTPUT_DIR ) {
	print "Making dir: $OUTPUT_DIR\n";
	system("mkdir -p $OUTPUT_DIR");
	system("chmod -R 775 $OUTPUT_DIR");
}

if ( -e $HTML_FORM ) {
	print "Deleting existing HTML file: $HTML_FORM\n";
	system("/bin/rm -f $HTML_FORM");
}

system("touch $HTML_FORM");

&print_html_form;

exit(0);
###################
#
# generate input table for PLATFORM/OS/OS_VER/TESTER 
#
##################


sub print_html_cfg {
	print OUTFILE "<TABLE BORDER=5 WIDTH=250>\n";
  	print OUTFILE "<TR> <TH WIDTH=95><P ALIGN=LEFT><BR></P></TH>\n";
	print OUTFILE "<TD WIDTH=121> <P ALIGN=CENTER STYLE=\"font-weight: medium\">Options</P> </TD>\n";
	print OUTFILE "<TH WIDTH=95><P ALIGN=CENTER STYLE=\"font-weight: medium\">Override</P></TH></TR>\n";
        print OUTFILE "<TR>\n";
open (CFG,"${CFG_INPUT}") or die "Cant open ${CFG_INPUT}\n";
while (<CFG>){
	if ( $_ !~ /^#|^$/) {
		@FIELDS=split /:/, $_;
		$GEN=splice(@FIELDS, 0, 1);
		print OUTFILE "<TR>\n";
		print OUTFILE "<TH WIDTH=113> <P ALIGN=LEFT>${GEN}</P></TH>\n";  
		print OUTFILE "<TD WIDTH=143> <DIV ALIGN=LEFT> <P><SELECT NAME=\"${GEN}\">\n";
		foreach $OPT (@FIELDS) {  print OUTFILE "<OPTION>${OPT}" ; } 
		print OUTFILE "</SELECT></P></DIV></TD>\n";
		print OUTFILE "<TH><INPUT NAME=\"${GEN}_OV\" SIZE=10></TH>\n";
		print OUTFILE "</TR>\n";
		}
	}
	print OUTFILE "</TR>\n";
	print OUTFILE "</TABLE>\n";
}


################################################################################
#
# print_html_form
#
################################################################################
sub print_html_form {

	local ( $assert, $testname, @asserts_files );


	open( OUTFILE, ">$HTML_FORM" ) || die "Cant open $HTML_FORM";

	print OUTFILE "<HTML>\n";
	print OUTFILE "<HEAD>\n";
	print OUTFILE "<TITLE>$MAJOR_TEST_ID $MINOR_TEST_ID tests</TITLE>\n";

	# A little bit of Javascript for the pop-up Instructions windows
	print OUTFILE "<SCRIPT LANGUAGE=\"Javascript\" ";
	print OUTFILE "TYPE=\"text/javascript\">\n";
	print OUTFILE "<!--\n";
	print OUTFILE "function instr_window(name, instr_url)\n";
	print OUTFILE "{\n";
	print OUTFILE "  win_name = window.open(instr_url,name,";
	print OUTFILE "'scrollbars=yes, resizable=yes, toolbar=no,";
	print OUTFILE "height=640,width=640');\n";
	print OUTFILE "  if (window.focus) win_name.focus();\n";
	print OUTFILE "\n";
	print OUTFILE "  if (win_name.opener == null)\n";
	print OUTFILE "    win_name.opener = window;\n";
	print OUTFILE "  win_name.opener.name = \"main\";\n";
	print OUTFILE "}\n";
	print OUTFILE "//-->\n";
	print OUTFILE "</SCRIPT>\n";

	print OUTFILE "</HEAD>\n\n";

	print OUTFILE "<BODY BGCOLOR=#ffffff>\n";
	print OUTFILE "<H1 align=center>$MAJOR_TEST_ID $MINOR_TEST_ID tests</H1>";
	print OUTFILE "Instructions:\n";
	print OUTFILE "<OL>\n";
	print OUTFILE "<LI>\n";
	print OUTFILE "<B>PLATFORM</B> specifies the underlying Operating System.\n";
	print OUTFILE "</LI>\n";
	print OUTFILE "<BR>\n<BR>\n";
	print OUTFILE "<LI>\n";
	print OUTFILE "<B>OS_VER</B> specifies the version of the Operating System.\n";
	print OUTFILE "</LI>\n";
	print OUTFILE "<BR>\n<BR>\n";
	print OUTFILE "<LI>\n";
	print OUTFILE "<B>BUILD_ID</B> is a unique identifier for the GNOME build.\n";
	print OUTFILE "</LI>\n";
	print OUTFILE "<BR>\n<BR>\n";
	print OUTFILE "<LI>\n";
	print OUTFILE "Enter <B>TESTER</B>'s User Id.\n";
	print OUTFILE "</LI>\n";
	print OUTFILE "<BR>\n<BR>\n";

	print OUTFILE "<LI>\n";
	print OUTFILE "<B>Text Input to the override option will override options selected in dropdown menus.</B>\n";
	print OUTFILE "</LI>\n";
	print OUTFILE "<BR>\n<BR>\n";

	print OUTFILE "<LI>\n";
	print OUTFILE "Read <B>instructions</B> for each test assertion and perform test.\n";
	print OUTFIEL "</LI>\n";
	print OUTFILE "<BR>\n<BR>\n";
	print OUTFILE "<LI>\n";
	print OUTFILE "Press <B>pass</B>, <B>fail</B> or <B>not done</B> buttons to record test results.\n";
	print OUTFILE "</LI>\n";
	print OUTFILE "<BR>\n<BR>\n";
	print OUTFILE "<LI>\n";
	print OUTFILE "Press <B>${RECORD_BUTTON_LABEL}</B> button when finished to save results.\n";
	print OUTFILE "</LI>\n";
	print OUTFILE "</OL>\n";
	print OUTFILE "<BR>\n<BR>\n";
	print OUTFILE "<I>View $MAJOR_TEST_ID $MINOR_TEST_ID <A HREF=\"javascript:instr_window('SmallWin2', '${TOC}')\"";
	print OUTFILE " onmouseover=\"window.status='${TOC}' ; return true \">table of contents.</A>.</I>\n";
	print OUTFILE "<HR>\n";
	print OUTFILE "<FORM ACTION=${ACTION_URL} ";
	print OUTFILE "METHOD=POST NAME=test_configurations>\n";
	print OUTFILE "<CENTER>\n";

	&print_html_cfg(); 

	print OUTFILE "<P>\n";
	print OUTFILE "<INPUT TYPE=hidden NAME=MAJOR_TEST_ID VALUE=${MAJOR_TEST_ID} SIZE=10></TH>\n";
	print OUTFILE "<INPUT TYPE=hidden NAME=MINOR_TEST_ID VALUE=${MINOR_TEST_ID} SIZE=10></TH>\n";
	print OUTFILE "<TABLE BORDER=5>\n";
	print OUTFILE "<TR>\n";
	print OUTFILE "<TH ALIGN=center VALIGN=bottom>Test Instructions</TH>\n";
	print OUTFILE "<TH ALIGN=center VLAIGN=bottom>Test Result</TH>\n";
	print OUTFILE "<TH ALIGN=center VLAIGN=bottom>Bugtraq</TH>\n";
	print OUTFILE "</TR>\n";

	print "Reading test files in: $INPUT_DIR\n";

	# process all the test assertion files in the $INPUT_DIR
	# directory, and add a row to the table for each test

	opendir( ASSERTS, $INPUT_DIR );
	# match files that DON'T start with "."
	@assert_files = grep( /.html$/, readdir( ASSERTS) );
	closedir( ASSERTS );
        @assert_files = sort ( @assert_files);
	# one row for each test assertion file
	foreach $assert ( @assert_files ) {
		if ( $assert =~ /index.html/ ) {
			print "skipping index.html\n";
			next;
		}

		if ( $assert =~ /_toc.html/ ) {
			print "skipping $assert\n";
			next;
		}

		$testname = $assert;
		print "Adding test: $assert\n";

		# strip the trailing ".html"
		$testname =~ s/\.html//;

		print OUTFILE "<TR>\n";
		print OUTFILE "<TD ALIGN=center VALIGN=bottom>\n";
		print OUTFILE "<A HREF=\"javascript:instr_window('SmallWin', '$assert')\"";
		print OUTFILE " onmouseover=\"window.status='$assert' ; return true\">";
		print OUTFILE "<B>Instructions</B></A> for ${testname}\n";
		print OUTFILE "</TD>\n";
		print OUTFILE "<TD ALIGN=center VALIGN=top ";
		print OUTFILE "BGCOLOR=\"\#ADD8E6\">\n";
		print OUTFILE "<INPUT TYPE=radio ";
		print OUTFILE "NAME=result_$testname ";
		print OUTFILE "VALUE=\"passed\"> pass\n";
		print OUTFILE "<INPUT TYPE=radio ";
		print OUTFILE "NAME=result_$testname ";
		print OUTFILE "VALUE=\"failed\"> fail\n";
		print OUTFILE "<INPUT TYPE=radio CHECKED ";
		print OUTFILE "NAME=result_$testname ";
		print OUTFILE "VALUE=\"not done\"> not done\n";
		print OUTFILE "</TD>\n";
		print OUTFILE "<TH><INPUT NAME=\"bugid_${testname}\" SIZE=10></TH>\n";
	    
		print OUTFILE "</TR>\n";
	}

	print OUTFILE "<TR>\n";
	print OUTFILE "<TD ALIGN=center COLSPAN=3>\n";
	print OUTFILE "<INPUT TYPE=\"submit\" ";
	print OUTFILE "VALUE=\"${RECORD_BUTTON_LABEL}\">\n";
	print OUTFILE "</TD>\n";
	print OUTFILE "</TR>\n";

        print OUTFILE "</TABLE>";
	print OUTFILE "</CENTER>\n";
	print OUTFILE "</FORM>\n";
	print OUTFILE "<P>\n";
	print OUTFILE "<HR>\n";
	print OUTFILE "<BR>\n";
	print OUTFILE "<BR>\n";
	print OUTFILE "<I>Page last updated: $DATE_TIME_STAMP</I>\n";
	print OUTFILE "</BODY>\n";
	print OUTFILE "</HTML>\n";

	close(OUTFILE);

	print "Wrote output HTML file: $HTML_FORM\n";
}
