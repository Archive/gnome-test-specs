#!/bin/sh

base_dir=".."
dir="$base_dir/evolution"
script="../../../scripts/mk_toc_naut.pl"

cd $dir
cd calendar
cd cal_create; $script cal_create "Evolution Calendar - Create"; cd ..
cd cal_time; $script cal_time "Evolution Calendar - Time_zone"; cd ..
cd cal_edit; $script cal_edit "Evolution Calendar - Edit"; cd ..
cd cal_file; $script cal_file "Evolution Calendar - File"; cd ..
cd cal_gen; $script cal_gen "Evolution Calendar - General"; cd ..
cd cal_help; $script cal_help "Evolution Calendar - Help"; cd ..
cd cal_print; $script cal_print "Evolution Calendar - Print"; cd ..
cd cal_task; $script cal_task "Evolution Calendar - Task"; cd ..
cd cal_shortcut; $script cal_shortcut "Evolution Calendar - Shortcut"; cd ..
cd cal_view; $script cal_view "Evolution Calendar - View"; cd ..
cd cal_actions; $script cal_actions "Evolution Calendar - Action"; cd ..
cd cal_tools; $script cal_tools "Evolution Calendar -Tools"; cd ..
cd ..
cd myevol 
cd myevol_gen; $script myevol_gen "Evolution Executive-summary - General"; cd ..
cd myevol_file; $script myevol_file "Evolution Executive-summary - File"; cd ..
cd myevol_tools; $script myevol_tools "Evolution Executive-summary - Tools"; cd ..
cd myevol_view; $script myevol_view "Evolution Executive-summary - View"; cd ..
cd myevol_help; $script myevol_help "Evolution Executive-summary - Help"; cd ..
cd ..
cd tasks
cd tasks_gen; $script tasks_gen "Evolution Tasks - General"; cd ..
cd tasks_file; $script tasks_file "Evolution Tasks - File"; cd ..
cd tasks_edit; $script tasks_edit "Evolution Tasks - Edit"; cd ..
cd tasks_view; $script tasks_view "Evolution Tasks - View"; cd ..
cd tasks_tools; $script tasks_tools "Evolution Tasks - Tools"; cd ..
cd tasks_help; $script tasks_help "Evolution Tasks - Help"; cd ..
cd ..
cd contacts
cd con_edit; $script con_edit "Evolution Contacts Manager - Edit"; cd ..
cd con_file; $script con_file "Evolution Contacts Manager - File"; cd ..
cd con_gen; $script con_gen "Evolution Contacts Manager - General"; cd ..
cd con_help; $script con_help "Evolution Contacts Manager - Help"; cd ..
cd con_new; $script con_new "Evolution Contacts Manager - Add/Delete/Edit"; cd ..
cd con_shortcut; $script con_shortcut "Evolution Contacts Manager - Shortcut"; cd ..
cd con_toolbar; $script con_toolbar "Evolution Contacts Manager - Toolbar"; cd ..
cd con_tools; $script con_tools "Evolution Contacts Manager - Tool"; cd ..
cd con_alter; $script con_alter "Evolution Contacts Manager - Alter Contact"; cd ..
cd con_view; $script con_view "Evolution Contacts Manager - View"; cd ..
cd ..
cd mail
cd mail_comp; $script mail_comp "Evolution Mail - Compose"; cd ..
cd mail_conf; $script mail_conf "Evolution Mail - Configure"; cd ..
cd mail_edit; $script mail_edit "Evolution Mail - Edit"; cd ..
cd mail_file; $script mail_file "Evolution Mail - File"; cd ..
cd mail_filter; $script mail_filter "Evolution Mail - Filter"; cd ..
cd mail_folder; $script mail_folder "Evolution Mail - Folder"; cd ..
cd mail_gen; $script mail_gen "Evolution Mail - General"; cd ..
cd mail_help; $script mail_help "Evolution Mail - Help"; cd ..
cd mail_message; $script mail_message "Evolution Mail - Message"; cd ..
cd mail_search; $script mail_search "Evolution Mail - Search"; cd ..
cd mail_tools; $script mail_tools "Evolution Mail - Tools"; cd ..
cd mail_actions; $script mail_actions "Evolution Mail - Actions"; cd ..
cd mail_shortcut; $script mail_shortcut "Evolution Mail - Shortcut"; cd ..
cd mail_tool; $script mail_tool "Evolution Mail - Tool"; cd ..
cd mail_virt; $script mail_virt "Evolution Mail - Virtual Folder"; cd ..
cd mail_view; $script mail_view "Evolution Mail - View"; cd ..
cd ..
cd regression
cd latest; $script regression "Evolution Regression Suite"; cd ..
cd ..

script="../../scripts/mk_toc_naut.pl"

cd hotkeys; $script hotkeys "Evolution HotKey"; cd ..
cd int; $script int "Evolution Integration"; cd ..
