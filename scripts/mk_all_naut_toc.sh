#!/bin/sh

base_dir=".."
dir="$base_dir/nautilus"
script="../../scripts/mk_toc_naut.pl"

cd $dir

cd regress; $script regress "Nautilus - Regression"; cd ..
cd sanity; $script sanity "Nautilus - Sanity"; cd ..

script="../../../scripts/mk_toc_naut.pl"

cd func
cd app; $script func_app "Nautilus - App"; cd ..
cd book; $script func_book "Nautilus - Book"; cd ..
cd edit; $script func_edit "Nautilus - Edit"; cd ..
cd file; $script func_file "Nautilus - File"; cd ..
cd go; $script func_go "Nautilus - Go"; cd ..
cd help; $script func_help "Nautilus - Help"; cd ..
cd sidebar; $script func_side "Nautilus - Side"; cd ..
cd user; $script func_user "Nautilus - User"; cd ..
cd view; $script func_view "Nautilus - View"; cd ..
cd zoom; $script func_zoom "Nautilus - Zoom"; cd ..
cd ..
