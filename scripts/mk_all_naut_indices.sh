#!/bin/sh

base_dir=".."
dir="$base_dir/nautilus"
script="../../scripts/make_index.pl"

cd $dir

cd regress; $script nautilus regress; cd ..
cd sanity; $script nautilus sanity; cd ..

script="../../../scripts/make_index.pl"

cd func
cd app; $script nautilus func_app; cd ..
cd book; $script nautilus func_book; cd ..
cd edit; $script nautilus func_edit; cd ..
cd file; $script nautilus func_file; cd ..
cd go; $script nautilus func_go; cd ..
cd help; $script nautilus func_help; cd ..
cd sidebar; $script nautilus func_side; cd ..
cd user; $script nautilus func_user; cd ..
cd view; $script nautilus func_view; cd ..
cd zoom; $script nautilus func_zoom; cd ..
cd ..
find . -name index.html -exec chmod 755 {} \;
