#!/usr/bin/perl
#
# mk_toc_interop.pl
#
#   Makes the _toc.html file summarizing the tests in a directory.
#
# 30-Jan-2001, dermot.mccluskey
#   initial version.
#
# 23-May-2001, peter foley
#   initial interop version, based on naut script
#

#
# Main routine
#

if ( length( $ARGV[1] ) == 0 ) {
	print "Usage: mk_toc_interop.pl product_name \"Product Description\"\n";
	exit 1;
}

# set up some global variables
$PRODUCT_NAME=$ARGV[0];
$PRODUCT_DESC=$ARGV[1];
$INPUT_DIR=".";
$OUTPUT_DIR=".";
$TOC="${PRODUCT_NAME}_toc.html";

if ( -e $TOC ) {
	print "Deleting existing HTML file: $TOC\n";
	system("/bin/rm -f $TOC");
}

system("touch $TOC");

&print_html_form;

exit(0);


################################################################################
#
# print_html_form
#
################################################################################
sub print_html_form {
	local ( $assert, $testname, @asserts_files );
	local ( $aim );

	open( OUTFILE, ">$TOC" ) || die "Cant open $TOC";

	print OUTFILE "<HTML>\n";
	print OUTFILE "<HEAD>\n";
	print OUTFILE "<TITLE>$PRODUCT_DESC Testing</TITLE>\n";
	print OUTFILE "</HEAD>\n\n";

	print OUTFILE "<BODY BGCOLOR=#ffffff>\n";
	print OUTFILE "<H1 align=center>$PRODUCT_DESC</H1>\n";
	print OUTFILE "<H1 align=center>Table of Contents</H1>\n";

	print OUTFILE "<CENTER>\n";
	print OUTFILE "<TABLE BORDER=1>\n";
	print OUTFILE "<TR>\n";
	print OUTFILE "<TH WIDTH=20%>Test Assertion</TH>\n";
	print OUTFILE "<TH WIDTH=80%>Test Aim</TH>\n";
	print OUTFILE "</TR>\n";

	print "Reading test files in: $INPUT_DIR\n";

	# process all the test assertion files in the $INPUT_DIR
	# directory, and add a row to the table for each test

	opendir( ASSERTS, $INPUT_DIR );
	# match files that end with .html
	@assert_files = grep( /.html$/, readdir( ASSERTS ) );
	closedir( ASSERTS );

	# one row for each test assertion file
	foreach $assert ( @assert_files ) {
		if ( $assert =~ /index.html/ ) {
			print "skipping index.html\n";
			next;
		}

		if ( $assert =~ /_toc.html/ ) {
			print "skipping $assert\n";
			next;
		}

		$testname = $assert;
		print "Adding test: $assert\n";

		# strip the trailing ".html"
		$testname =~ s/\.html//;

		$aim = &get_test_aim( $assert );
		if ( $aim eq "" ) {
			$aim = "?????";
			print "*****ERROR: Unable to get Aim for [$assert]\n";
		}

		print OUTFILE "<TR>\n";
		print OUTFILE "<TD VALIGN=top>";
		print OUTFILE "${testname}";
		print OUTFILE "</TD>\n";
		print OUTFILE "<TD VALIGN=top>";
		print OUTFILE "${aim}";
		print OUTFILE "</TD>\n";
		print OUTFILE "</TR>\n";
	}

        print OUTFILE "</TABLE>";
	print OUTFILE "</CENTER>\n";
	print OUTFILE "<P>\n";
	print OUTFILE "</BODY>\n";
	print OUTFILE "</HTML>\n";

	close(OUTFILE);

	print "Wrote output HTML file: $TOC\n";
}

sub get_test_aim
{
	my( $testfile ) = ( @_ );
	my( $state, $line, $aim );

	
	open( TESTFILE, "$testfile" ) || die "cannot open $testfile: \n";

	$aim = "";
	$state = 1;
	while( <TESTFILE> ) {
		chomp;
		$line = $_;
		if ( $state == 1 ) {
			if ( $line =~ /<B>Description.*<\/B>/i ) {
				$state = 2;
				next;
			}
		}

		if ( $state == 2 ) {
			if ( $line =~ /\w*<P.*?>(.*?)<\/P>/i ) {
				$aim = $1;
				$state = 3;
				next;
			}
		}
	}

	if ( $state == 1 ) {
		print "*****WARN: Never found Description in $testfile\n";
	}

	if ( $state == 2 ) {
		print "*****WARN: Never found Test Aim in $testfile\n";
	}

	close( TESTFILE );
	return( $aim );
}
