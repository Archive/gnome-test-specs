/*
 * TextDnD
 *
 * Mark Murnane  <Mark.Murnane@ireland.sun.com>
 *
 * Java Application implementing Drag and Drop for the TextArea
 */

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.awt.event.*;
import java.io.*;
import javax.swing.*;



public class TextDnD 
extends JTextArea 
implements DragGestureListener, DragSourceListener, DropTargetListener {
    DragSource dragSource;
    JLabel infoBox;
    
    public static void main (String[] args) {
        JFrame f = new JFrame("Drag and Drop!!");
        TextDnD text = new TextDnD();
          
        
        // Set up the Frame
        f.setSize (300,300);
        f.setLocation (100,100);
        f.addWindowListener (new WindowAdapter () {
            public void windowClosing(WindowEvent we) { System.exit(0); }
        });
         
        f.getContentPane().add(new JScrollPane(text), BorderLayout.CENTER);
        f.getContentPane().add(text.infoBox, BorderLayout.SOUTH);
        
     
        f.setVisible(true);
    }
    
    public TextDnD() {
		infoBox = new JLabel("Mouse Pointer: ");

		// Setup the drag source information.
		dragSource = DragSource.getDefaultDragSource();
		dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY_OR_MOVE, this);

        // Set this component up as a drop target							   
        setDropTarget(new DropTarget(this, this));
    }


  
/*********************************************************************************  
 *
 * Methods required for the DragSourceListener interface
 *
 *********************************************************************************/

    public void dragGestureRecognized(DragGestureEvent dsde) { 
        dsde.startDrag(DragSource.DefaultCopyDrop, 
                       new StringSelection(this.getSelectedText()),
                       this);
    }
    
    public void dragExit(DragSourceEvent dsde) {
    }
    
    public void dragDropEnd(DragSourceDropEvent dsde) {
    }
    
    public void dropActionChanged(DragSourceDragEvent dsde) {
    }
    
    public void dragOver(DragSourceDragEvent dsde) {
    }
    
    public void dragEnter(DragSourceDragEvent dsde) {
    }
    


/*********************************************************************************  
 *
 * Methods required for the DropTargetListener interface
 *
 *********************************************************************************/
    
    public void dragOver(DropTargetDragEvent dtde) {
        infoBox.setText("Mouse Pointer:    " + dtde.getLocation().x
                            + ", " + dtde.getLocation().y);
    }
    
    public void dropActionChanged(DropTargetDragEvent dtde) {
    }
    
    public void dragEnter(DropTargetDragEvent dtde) {
        
        try {
            if(dtde.isDataFlavorSupported(DataFlavor.plainTextFlavor) ||
                dtde.isDataFlavorSupported(
                    new DataFlavor("text/plain; class=java.io.InputStream; charset=ascii")) ||
				dtde.isDataFlavorSupported(
                    new DataFlavor("text/plain; class=java.io.InputStream; charset=iso8859-1"))) {
                dtde.acceptDrag(DnDConstants.ACTION_COPY_OR_MOVE);
                this.setBackground(Color.lightGray);
            }
            else {
                DataFlavor[] dfs;
            
                dfs = dtde.getCurrentDataFlavors();
            
                for(int i = 0; i < dfs.length; i++) {
                    System.out.println("Supported Flavour: " + dfs[i].getMimeType());
                }
            
                this.setBackground(Color.red);
            }
        }
        catch (ClassNotFoundException cnfe) {
            System.out.println("Unable to create text/plain MIME type: " + cnfe);
        }
    }
    
    public void drop(DropTargetDropEvent dtde) {
        Transferable t = dtde.getTransferable();
        DataFlavor textPlain;
		DataFlavor iso8859;
        
        try {
            textPlain = new DataFlavor("text/plain; class=java.io.InputStream; charset=ascii");
			iso8859 = new DataFlavor("text/plain; class=java.io.InputStream; charset=iso8859-1");
        }
        catch (ClassNotFoundException cnfe) {
            System.out.println("Unable to create text/plain MIME type: " + cnfe);
            return;
        }
        
        
        if(t.isDataFlavorSupported(DataFlavor.plainTextFlavor) ||
            t.isDataFlavorSupported(textPlain) ||
		    t.isDataFlavorSupported(iso8859)) {  
            dtde.acceptDrop(DnDConstants.ACTION_COPY_OR_MOVE);
 
            
            DataFlavor df = DataFlavor.selectBestTextFlavor(
                                                t.getTransferDataFlavors());
                
            if (df != null) {
                Reader dropData;
				
                
                try {
                    Object source = df.getReaderForText(t);
                
                    if(source instanceof InputStreamReader) {
                        dropData = (InputStreamReader)source;
                    }
					else if (source instanceof StringReader) {
						dropData = (StringReader)source;
					}
                    else {
                        System.out.println("Unable to use InputStreamReader");
						System.out.println("Use: " + source.getClass().getName());
                        dtde.dropComplete(false);
                        return;
                    }
                }
                catch (Exception e) {
                    System.out.println("Exception while obtaining InputStream: " + e);
                    dtde.dropComplete(false);
                    return;
                }
                
                
                // Now that we have the InputStreamReader established
                // we can read the data it has
                try { 
                    int item;
                    
					System.out.println("Stream " + (dropData.ready() ? "is" : "isn't") + " ready");
                    
                    // Now to start reading characters
					while((item = dropData.read()) != -1) 
						this.append(String.valueOf((char)item));

                    
                    System.out.print("\n");
                    System.out.println("Finished reading from the stream");
                    dropData.close();
                    
                    // Successfully completed
                    dtde.dropComplete(true);
                }
                catch (Exception e) {
                    System.out.println("Exception accessing the InputStream: " + e);
                    dtde.dropComplete(false);
                    return;
                }
            }
        } 
    }
    
    public void dragExit(DropTargetEvent dtde) {
        this.setBackground(Color.white);
    }
    
}
